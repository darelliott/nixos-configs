# Readme

This repo contains my personal nixos configs.
I have at present three machines running nixos:

- optiplex3020: An end user machine with a graphical user interface.
- swift3: My laptop, with a very similar setup to optiplex.
- rpi4: A rasperry pi 4 configured as a headless server for my self-hosted services.

## Layout

The tree of the repo at time of writing can be seen below:

```
.
├── home-manager # Home manager configurations
│   ├── common # Used by all computers
│   │   └── base
│   └── desktop # Used only by graphical computers.
│       ├── base
│       │   ├── helix-extras
│       │   ├── scripts
│       │   └── wallpapers
│       └── old # Modules no longer in use, but may be used again at some point.
├── hosts # Configuration specific to a computer or generated image
│   ├── install-iso
│   ├── optiplex3020
│   ├── rpi4
│   └── swift3
├── lib # Helper functions
├── modules # Modules that may or may not be fit for upstreaming.
│   ├── home-manager
│   │   └── variables # Non upstreamabale module used for setting miscellanious variables.
│   └── nixos
│       └── ddns-updater
├── nixos # Configuration for nixos
│   ├── common # Used by all computers
│   │   ├── base # Used by all computers
│   │   └── optional # Optional, but suitable for all computers. Often stateful, and requiring manual setup pre activation
│   ├── desktop # For graphical environments
│   │   ├── base
│   │   └── optional
│   └── server # For servers
│       ├── base
│       └── optional
├── overlays # Overlays to add packages from pkgs and patch certain packages.
├── pkgs # Custom packages, some to be/in the process of being upstreamed.
│   ├── ddns-updater
│   ├── install-iso
│   └── kidex
├── secrets # Secrets, managed using sops-nix
└── templates # Flake templates for development.
    ├── cpp
    └── python
```

The structure is quite verbose, and hence reasonably self-explanatory.

I only use nix on nixos, so some of my home manager files have dependencies on nix config options, or merely configure things that are actually installed system-wide through nixos options. Additionally, as I am the only user it is possible to have nixos options directly reference home-manager config options, for example for theming the console based on my current theme, as seen in console.nix. I am aware this is something of an antipattern, so it is avoided where possible.

## Philosophy

I prefer to have settings pertinent to each option or service in one file and then use conditionals to activate segments rather than spreading it throughout files. For example, /var/lib/iwd must be persisted by impermanence if iwd is used. Rather than adding an impermanence option to networking-wireless.nix I instead use a condition on iwd being enabled in impermanence.nix. I find it makes it easier to keep track of modules and options.

## Acknowledgements

Much of layout is inspired by ![Misterio 77's nix-config](https://github.com/Misterio77/nix-config).
My thanks to all people who have worked to make nix as wonderful an ecosystem as it is today.
