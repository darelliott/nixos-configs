{
  inputs = {
    nixpkgs = {
      url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    };
    nixos-hardware = {
      url = "github:NixOS/nixos-hardware/master";
    };
    treefmt-nix = {
      url = "github:numtide/treefmt-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    impermanence = {
      url = "github:nix-community/impermanence";
    };
    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.3.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    anyrun = {
      url = "github:Kirottu/anyrun";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ironbar = {
      url = "github:JakeStanger/ironbar";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-colors = {
      url = "github:misterio77/nix-colors";
    };
    nu_scripts = {
      url = "github:nushell/nu_scripts";
      flake = false;
    };
    ddns-updater-pull = {
      url = "github:delliottxyz/nixpkgs/ddns-updater";
    };
  };

  outputs =
    inputs:
    let
      lib = inputs.nixpkgs.lib // inputs.home-manager.lib;
      libx = import ./lib { inherit inputs; };
      treefmtEval = libx.forEachSystem (
        pkgs:
        inputs.treefmt-nix.lib.evalModule pkgs {
          config = {
            # Used to find the project root
            projectRootFile = "flake.nix";
            programs = {
              nixfmt = {
                enable = true;
                package = pkgs.nixfmt-rfc-style;
                # package = pkgs.nixfmt;
              };
              deadnix.enable = true;
              statix.enable = true;
              shellcheck.enable = true;
              shfmt.enable = true;
              prettier = {
                enable = true;
                includes = [
                  "*.md"
                  "*.yaml"
                ];
              };
            };
            settings.formatter.nixfmt = {
              options = [ "--verify" ];
            };
          };
        }
      );
    in
    {
      inherit lib libx;
      nixosModules = import ./modules/nixos;
      homeManagerModules = import ./modules/home-manager;
      templates = import ./templates;
      overlays = import ./overlays { inherit inputs; };
      packages = libx.forEachSystem (pkgs: import ./pkgs { inherit inputs pkgs; });
      devShells = libx.forEachSystem (pkgs: import ./shell.nix { inherit pkgs; });
      # for `nix fmt`
      formatter = libx.forEachSystem (pkgs: treefmtEval.${pkgs.system}.config.build.wrapper);
      # for `nix flake check`
      checks = libx.forEachSystem (
        pkgs: { formatting = treefmtEval.${pkgs.system}.config.build.check inputs.self; }
      );

      nixosConfigurations = {
        swift3 = inputs.nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          }; # allows access to flake inputs in nixos modules
          modules = [ ./hosts/swift3 ];
        };
        optiplex3020 = inputs.nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          specialArgs = {
            inherit inputs;
          }; # allows access to flake inputs in nixos modules
          modules = [ ./hosts/optiplex3020 ];
        };
        rpi4 = inputs.nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          specialArgs = {
            inherit inputs;
          }; # allows access to flake inputs in nixos modules
          modules = [ ./hosts/rpi4 ];
        };
      };
    };
}
