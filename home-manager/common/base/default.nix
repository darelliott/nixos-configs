{ ... }:
{
  imports = [
    ./home.nix
    ./shell.nix
    ./xdg.nix
    ./helix.nix
    ./theming.nix
  ];
}
