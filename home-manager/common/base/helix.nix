{ config, pkgs, ... }:
{
  home.sessionVariables = {
    EDITOR = "hx";
    VISUAL = "hx";
  };
  # Enables system wide copy paste, perhaps not needed with full DE
  home.packages = with pkgs; [ wl-clipboard ];
  programs.helix = {
    enable = true;
    defaultEditor = true;
    extraPackages = with pkgs; [
      nil
      nixfmt
      nodePackages.bash-language-server
      shellcheck
      shfmt
    ];
    settings = {
      theme = "base16_transparent";
      editor = {
        auto-completion = true;
        auto-format = true;
        auto-save = true;
        line-number = "relative";
        undercurl = true;
        true-color = true;
        shell = [
          "${pkgs.nushell}/bin/nu"
          "-c"
        ];
        lsp = {
          enable = true;
          snippets = true;
          display-messages = true;
          auto-signature-help = true;
          display-inlay-hints = true;
          display-signature-help-docs = true;
          goto-reference-include-declaration = true;
        };
        indent-guides = {
          render = true;
        };
        soft-wrap = {
          enable = true;
        };

        whitespace = {
          render = {
            space = "none";
            tab = "none";
            newline = "all";
          };
        };
      };
    };
    languages = {
      language = [
        {
          name = "nix";
          language-servers = [ "nil" ];
          file-types = [
            "nix"
            "flake"
          ];
          scope = "source.nix";
          roots = [ "flake.nix" ];
          auto-format = false;
          formatter = {
            command = "nixfmt";
          };
        }
        {
          name = "bash";
          auto-format = true;
          formatter = {
            command = "shfmt";
          };
        }
      ];
      language-server = {
        nil = {
          command = "nil";
          config = { };
        };
      };
    };
    themes = {
      nix-colors =
        let
          base00 = "#${config.colorScheme.palette.base00}"; # Default Background
          base01 = "#${config.colorScheme.palette.base01}"; # Lighter Background (Used for status bars, line number and folding marks)
          base02 = "#${config.colorScheme.palette.base02}"; # Selection Background
          base03 = "#${config.colorScheme.palette.base03}"; # Comments, Invisibles, Line Highlighting
          base04 = "#${config.colorScheme.palette.base04}"; # Dark Foreground (Used for status bars)
          base05 = "#${config.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators
          base06 = "#${config.colorScheme.palette.base06}"; # Light Foreground (Not often used)
          base07 = "#${config.colorScheme.palette.base07}"; # Light Background (Not often used)
          base08 = "#${config.colorScheme.palette.base08}"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
          base09 = "#${config.colorScheme.palette.base09}"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url
          base0A = "#${config.colorScheme.palette.base0A}"; # Classes, Markup Bold, Search Text Background
          base0B = "#${config.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted
          base0C = "#${config.colorScheme.palette.base0C}"; # Support, Regular Expressions, Escape Characters, Markup Quotes
          base0D = "#${config.colorScheme.palette.base0D}"; # Functions, Methods, Attribute IDs, Headings
          base0E = "#${config.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
          base0F = "#${config.colorScheme.palette.base0F}"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>    "attributes" = base09k
        in
        {
          "comment" = {
            fg = base04;
            modifiers = [ "italic" ];
          };
          "constant" = base09;
          "constant.character.escape" = base0C;
          "constant.numeric" = base09;
          "constructor" = base0D;
          "debug" = base03;
          "diagnostic" = {
            modifiers = [ "underlined" ];
          };
          "diff.delta" = base09;
          "diff.minus" = base08;
          "diff.plus" = base0B;
          "error" = base08;
          "function" = base0D;
          "hint" = base03;
          "info" = base0D;
          "keyword" = base0E;
          "label" = base0E;
          "namespace" = base0E;
          "operator" = base05;
          "special" = base0D;
          "string" = base0B;
          "type" = base0A;
          "variable" = base0D;
          "variable.other.member" = base0B;
          "warning" = base09;

          "markup.bold" = {
            fg = base0A;
            modifiers = [ "bold" ];
          };
          "markup.heading" = base0D;
          "markup.italic" = {
            fg = base0E;
            modifiers = [ "italic" ];
          };
          "markup.link.text" = base08;
          "markup.link.url" = {
            fg = base09;
            modifiers = [ "underlined" ];
          };
          "markup.list" = base08;
          "markup.quote" = base0C;
          "markup.raw" = base0B;
          "markup.strikethrough" = {
            modifiers = [ "crossed_out" ];
          };

          "diagnostic.hint" = {
            underline = {
              style = "curl";
            };
          };
          "diagnostic.info" = {
            underline = {
              style = "curl";
            };
          };
          "diagnostic.warning" = {
            underline = {
              style = "curl";
            };
          };
          "diagnostic.error" = {
            underline = {
              style = "curl";
            };
          };

          "ui.background" = {
            bg = base00;
          };
          "ui.bufferline.active" = {
            fg = base00;
            bg = base03;
            modifiers = [ "bold" ];
          };
          "ui.bufferline" = {
            fg = base04;
            bg = base00;
          };
          "ui.cursor" = {
            fg = base0A;
            modifiers = [ "reversed" ];
          };
          "ui.cursor.insert" = {
            fg = base0A;
            modifiers = [ "reversed" ];
          };
          "ui.cursorline.primary" = {
            fg = base05;
            bg = base01;
          };
          "ui.cursor.match" = {
            fg = base0A;
            modifiers = [ "reversed" ];
          };
          "ui.cursor.select" = {
            fg = base0A;
            modifiers = [ "reversed" ];
          };
          "ui.gutter" = {
            bg = base00;
          };
          "ui.help" = {
            fg = base06;
            bg = base01;
          };
          "ui.linenr" = {
            fg = base03;
            bg = base00;
          };
          "ui.linenr.selected" = {
            fg = base04;
            bg = base01;
            modifiers = [ "bold" ];
          };
          "ui.menu" = {
            fg = base05;
            bg = base01;
          };
          "ui.menu.scroll" = {
            fg = base03;
            bg = base01;
          };
          "ui.menu.selected" = {
            fg = base07;
            bg = base04;
          };
          "ui.popup" = {
            bg = base01;
          };
          "ui.selection" = {
            bg = base02;
          };
          "ui.selection.primary" = {
            bg = base02;
          };
          "ui.statusline" = {
            fg = base04;
            bg = base01;
          };
          "ui.statusline.inactive" = {
            bg = base01;
            fg = base03;
          };
          "ui.statusline.insert" = {
            fg = base00;
            bg = base0B;
          };
          "ui.statusline.normal" = {
            fg = base00;
            bg = base03;
          };
          "ui.statusline.select" = {
            fg = base00;
            bg = base0F;
          };
          "ui.text" = base05;
          "ui.text.focus" = base05;
          "ui.virtual.indent-guide" = {
            fg = base03;
          };
          "ui.virtual.inlay-hint" = {
            fg = base01;
          };
          "ui.virtual.ruler" = {
            bg = base01;
          };
          "ui.window" = {
            bg = base01;
          };
        };
    };
  };
}
