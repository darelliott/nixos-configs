{ pkgs, ... }:
{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home = {
    username = "dae";
    homeDirectory = "/home/dae";
    sessionVariables = { };

    packages = with pkgs; [
      hugo
      gtypist
      zip
      unzip
      mpv
      felix-fm
      chafa
      processing
    ];
  };

  programs.git = {
    enable = true;
    userEmail = "me@delliott.xyz";
    userName = "Darragh Elliott";
  };

  systemd.user.tmpfiles.rules = [ "L /home/dae/nixos - - - - /etc/nixos" ];

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "24.05";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
