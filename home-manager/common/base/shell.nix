{
  inputs,
  lib,
  pkgs,
  config,
  osConfig,
  ...
}:
{
  home.packages = with pkgs; [
    ripgrep
    bottom
    dua
    glow
    uutils-coreutils-noprefix
    nix-tree
  ];

  programs = {
    bash = {
      enable = true;
      historyFile = "${config.xdg.configHome}/bash/.bash_history";
    };
    nushell = {
      enable = true;
      extraConfig = ''
        # let's define some colors

        let base00 = "#${config.colorScheme.palette.base00}" # Default Background
        let base01 = "#${config.colorScheme.palette.base01}" # Lighter Background (Used for status bars, line number and folding marks)
        let base02 = "#${config.colorScheme.palette.base02}" # Selection Background
        let base03 = "#${config.colorScheme.palette.base03}" # Comments, Invisibles, Line Highlighting
        let base04 = "#${config.colorScheme.palette.base04}" # Dark Foreground (Used for status bars)
        let base05 = "#${config.colorScheme.palette.base05}" # Default Foreground, Caret, Delimiters, Operators
        let base06 = "#${config.colorScheme.palette.base06}" # Light Foreground (Not often used)
        let base07 = "#${config.colorScheme.palette.base07}" # Light Background (Not often used)
        let base08 = "#${config.colorScheme.palette.base08}" # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
        let base09 = "#${config.colorScheme.palette.base09}" # Integers, Boolean, Constants, XML Attributes, Markup Link Url
        let base0a = "#${config.colorScheme.palette.base0A}" # Classes, Markup Bold, Search Text Background
        let base0b = "#${config.colorScheme.palette.base0B}" # Strings, Inherited Class, Markup Code, Diff Inserted
        let base0c = "#${config.colorScheme.palette.base0C}" # Support, Regular Expressions, Escape Characters, Markup Quotes
        let base0d = "#${config.colorScheme.palette.base0D}" # Functions, Methods, Attribute IDs, Headings
        let base0e = "#${config.colorScheme.palette.base0E}" # Keywords, Storage, Selector, Markup Italic, Diff Changed
        let base0f = "#${config.colorScheme.palette.base0F}" # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>

        # we're creating a theme here that uses the colors we defined above.

        let base16_theme = {
            separator: $base03
            leading_trailing_space_bg: $base04
            header: $base0b
            date: $base0e
            filesize: $base0d
            row_index: $base0c
            bool: $base08
            int: $base0b
            duration: $base08
            range: $base08
            float: $base08
            string: $base04
            nothing: $base08
            binary: $base08
            cellpath: $base08
            hints: dark_gray

            # shape_garbage: { fg: $base07 bg: $base08 attr: b} # base16 white on red
            # but i like the regular white on red for parse errors
            shape_garbage: { fg: "#FFFFFF" bg: "#FF0000" attr: b}
            shape_bool: $base0d
            shape_int: { fg: $base0e attr: b}
            shape_float: { fg: $base0e attr: b}
            shape_range: { fg: $base0a attr: b}
            shape_internalcall: { fg: $base0c attr: b}
            shape_external: $base0c
            shape_externalarg: { fg: $base0b attr: b}
            shape_literal: $base0d
            shape_operator: $base0a
            shape_signature: { fg: $base0b attr: b}
            shape_string: $base0b
            shape_filepath: $base0d
            shape_globpattern: { fg: $base0d attr: b}
            shape_variable: $base0e
            shape_flag: { fg: $base0d attr: b}
            shape_custom: {attr: b}
        }
        $env.config = {
          show_banner:  false,
          color_config: $base16_theme # <-- this is the theme
          use_grid_icons: true
          rm: {
              always_trash: false # always act as if -t was given. Can be overridden with -p
          }
          filesize: {
              metric: true # true => KB, MB, GB (ISO standard), false => KiB, MiB, GiB (Windows standard)
          }        
        }
        source ${inputs.nu_scripts}/custom-completions/nix/nix-completions.nu
        source ${inputs.nu_scripts}/custom-completions/git/git-completions.nu
        source ${inputs.nu_scripts}/custom-completions/glow/glow-completions.nu
        source ${inputs.nu_scripts}/custom-completions/typst/typst-completions.nu
        source ${inputs.nu_scripts}/custom-completions/man/man-completions.nu
      '';
      shellAliases = lib.mkMerge [
        { "nr" = "sudo nixos-rebuild"; }
        (lib.mkIf (osConfig.networking.hostName == "swift3") { "btm" = "btm --battery"; })
      ];
    };

    starship = {
      enable = true;
      enableNushellIntegration = true;
      enableBashIntegration = false;
      settings = {
        shlvl = {
          disabled = false;
          format = "[$symbol]($style)";
          repeat = true;
          symbol = "❯";
          repeat_offset = 0;
          threshold = 0;
        };
      };
    };

    direnv = {
      enable = true;
      enableNushellIntegration = true;
      enableBashIntegration = false;
      nix-direnv.enable = true;
    };
  };
  programs.bat = {
    enable = true;
    config = {
      theme = "base16";
    };
  };
}
