{
  pkgs,
  inputs,
  config,
  osConfig,
  ...
}:
{
  imports = [
    inputs.nix-colors.homeManagerModules.default
    inputs.self.homeManagerModules.variables
  ];

  variables = {
    # use this to convert pergentage to hex https://davidwalsh.name/hex-opacity
    opacity = {
      number = 0.8;
      hex = "CC";
    };
    border = {
      radius.int = 10;
      width.wide.int = 2;
      width.narrow.int = 1;
    };
    gaps = {
      inner.int = 2;
      outer.int = 6;
    };
    colorScheme = {
      slug = "${config.colorScheme.slug}Custom";
      name = "${config.colorScheme.name}Custom";
      inherit (config.colorScheme) author;
      palette = {
        base00 = "${config.colorScheme.palette.base00}${config.variables.opacity.hex}"; # Default Background
        base01 = "${config.colorScheme.palette.base01}FF"; # Lighter Background (Used for status bars, line number and folding marks)
        base02 = "${config.colorScheme.palette.base02}FF"; # Selection Background
        base03 = "${config.colorScheme.palette.base03}FF"; # Comments, Invisibles, Line Highlighting
        base04 = "${config.colorScheme.palette.base04}FF"; # Dark Foreground (Used for status bars)
        base05 = "${config.colorScheme.palette.base05}FF"; # Default Foreground, Caret, Delimiters, Operators
        base06 = "${config.colorScheme.palette.base06}FF"; # Light Foreground (Not often used)
        base07 = "${config.colorScheme.palette.base07}${config.variables.opacity.hex}"; # Light Background (Not often used)
        base08 = "${config.colorScheme.palette.base08}FF"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
        base09 = "${config.colorScheme.palette.base09}FF"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url
        base0A = "${config.colorScheme.palette.base0A}FF"; # Classes, Markup Bold, Search Text Background
        base0B = "${config.colorScheme.palette.base0B}FF"; # Strings, Inherited Class, Markup Code, Diff Inserted
        base0C = "${config.colorScheme.palette.base0C}FF"; # Support, Regular Expressions, Escape Characters, Markup Quotes
        base0D = "${config.colorScheme.palette.base0D}FF"; # Functions, Methods, Attribute IDs, Headings
        base0E = "${config.colorScheme.palette.base0E}FF"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
        base0F = "${config.colorScheme.palette.base0F}FF"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>
      };
    };
  };

  # colorScheme = let
  #   nix-colors-lib = inputs.nix-colors.lib.contrib {inherit pkgs;};
  # in
  #   nix-colors-lib.colorSchemeFromPicture {
  #     path = ../../desktop/base/wallpapers/wallpaper.jpg;
  #     kind = "dark";
  #   };
  colorScheme = inputs.nix-colors.colorSchemes.dracula;

  #  base00 = "#${config.colorScheme.palette.base00}"; # Default Background
  #  base01 = "#${config.colorScheme.palette.base01}"; # Lighter Background (Used for status bars, line number and folding marks)
  #  base02 = "#${config.colorScheme.palette.base02}"; # Selection Background
  #  base03 = "#${config.colorScheme.palette.base03}"; # Comments, Invisibles, Line Highlighting
  #  base04 = "#${config.colorScheme.palette.base04}"; # Dark Foreground (Used for status bars)
  #  base05 = "#${config.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators
  #  base06 = "#${config.colorScheme.palette.base06}"; # Light Foreground (Not often used)
  #  base07 = "#${config.colorScheme.palette.base07}"; # Light Background (Not often used)
  #  base08 = "#${config.colorScheme.palette.base08}"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
  #  base09 = "#${config.colorScheme.palette.base09}"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url
  #  base0A = "#${config.colorScheme.palette.base0A}"; # Classes, Markup Bold, Search Text Background
  #  base0B = "#${config.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted
  #  base0C = "#${config.colorScheme.palette.base0C}"; # Support, Regular Expressions, Escape Characters, Markup Quotes
  #  base0D = "#${config.colorScheme.palette.base0D}"; # Functions, Methods, Attribute IDs, Headings
  #  base0E = "#${config.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
  #  base0F = "#${config.colorScheme.palette.base0F}"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>

  # This options tries to install the cursor files to ~/.local/share/icons, which causes issues with the flatpak mounts
  # Setting it in the gtk settings does not have this issue
  # home.pointerCursor = {
  #   package = pkgs.bibata-cursors;
  #   name = "Bibata-Modern-Ice";
  #   size = 32;
  #   gtk.enable = true;
  #   x11.enable = true;
  # };

  gtk = {
    enable = true;
    theme = {
      # Unfortunately, it seems most applications dont support hex with opacity.
      package =
        let
          nix-colors-lib = inputs.nix-colors.lib.contrib { inherit pkgs; };
        in
        nix-colors-lib.gtkThemeFromScheme { scheme = config.colorScheme; };
      name = "${config.colorScheme.slug}";
    };
    iconTheme = {
      package = pkgs.candy-icons;
      name = "candy-icons";
    };
    cursorTheme = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
    };
    font = {
      package = pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; };
      name = "${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif}";
      size = 12;
    };
  };
  # Attempts to match qt style to gtk style. Works quite well, although may struggle with transparency
  qt = {
    enable = true;
    platformTheme = "gtk";
    style = {
      name = "gtk2";
      package = pkgs.qt6Packages.qt6gtk2;
    };
  };
  # QT_STYLE_OVERRIDE should not be set as per the theme instructions: https://github.com/trialuser02/qt6gtk2
  # It is set in the home manager qt module. Attempts to remove it runs into a recursion issue.
}
