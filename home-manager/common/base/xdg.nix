{ config, ... }:
{
  xdg =
    let
      home = config.home.homeDirectory;
    in
    {
      enable = true;
      cacheHome = "${home}/.cache";
      configHome = "${home}/.config";
      dataHome = "${home}/.local/share";
      stateHome = "${home}/.local/state";
      userDirs = {
        enable = true;
        createDirectories = true;
        desktop = "${home}/Desktop";
        documents = "${home}/Documents";
        download = "${home}/Downloads";
        music = "${home}/Music";
        pictures = "${home}/Pictures";
        publicShare = "${home}/Public";
        templates = "${home}/Templates";
        videos = "${home}/Videos";
      };
      systemDirs = {
        config = [ "/etc/xdg" ];
        data = [ "/usr/share" ];
      };
      # xdg-mime seems to be a disaster to get setup and working properly
      # I think flatpak and the xdg-portal may be interfering?
      # Really not sure
      mime.enable = true;
      mimeApps = {
        enable = true;
        associations.added = {
          "image/png" = "org.gnome.Loupe.desktop";
          "image/jpeg" = "org.gnome.Loupe.desktop";
          "image/gif" = "org.gnome.Loupe.desktop";
          "text/plain" = "Helix.desktop";
          "text/*" = "Helix.desktop";
          "inode/directory" = "org.wezfurlong.wezterm.desktop";
          "x-scheme-handler/http" = "org.mozilla.firefox.desktop";
          "x-scheme-handler/https" = "org.mozilla.firefox.desktop";
          "x-scheme-handler/chrome" = "org.mozilla.firefox.desktop";
          "text/html" = "org.mozilla.firefox.desktop";
          "application/x-extension-htm" = "org.mozilla.firefox.desktop";
          "application/x-extension-html" = "org.mozilla.firefox.desktop";
          "application/x-extension-shtml" = "org.mozilla.firefox.desktop";
          "application/xhtml+xml" = "org.mozilla.firefox.desktop";
          "application/x-extension-xhtml" = "org.mozilla.firefox.desktop";
          "application/x-extension-xht" = "org.mozilla.firefox.desktop";
          "application/pdf" = "org.pwmt.zathura-pdf-mupdf.desktop";
        };
        defaultApplications = {
          "image/png" = [ "org.gnome.Loupe.desktop" ];
          "image/jpeg" = [ "org.gnome.Loupe.desktop" ];
          "image/gif" = [ "org.gnome.Loupe.desktop" ];
          "text/plain" = [ "Helix.desktop" ];
          "text/*" = [ "Helix.desktop" ];
          "inode/directory" = [ "org.wezfurlong.wezterm.desktop" ];
          "x-scheme-handler/http" = [ "org.mozilla.firefox.desktop" ];
          "x-scheme-handler/https" = [ "org.mozilla.firefox.desktop" ];
          "x-scheme-handler/chrome" = [ "org.mozilla.firefox.desktop" ];
          "text/html" = [ "org.mozilla.firefox.desktop" ];
          "application/x-extension-htm" = [ "org.mozilla.firefox.desktop" ];
          "application/x-extension-html" = [ "org.mozilla.firefox.desktop" ];
          "application/x-extension-shtml" = [ "org.mozilla.firefox.desktop" ];
          "application/xhtml+xml" = [ "org.mozilla.firefox.desktop" ];
          "application/x-extension-xhtml" = [ "org.mozilla.firefox.desktop" ];
          "application/x-extension-xht" = [ "org.mozilla.firefox.desktop" ];
          "application/pdf" = [ "org.pwmt.zathura-pdf-mupdf.desktop" ];
        };
      };
    };
}
