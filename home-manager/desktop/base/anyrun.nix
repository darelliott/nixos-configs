{
  config,
  inputs,
  pkgs,
  osConfig,
  ...
}:
{
  # https://github.com/Kirottu/anyrun
  imports = [ inputs.anyrun.homeManagerModules.default ];
  # home.packages = with pkgs; [ kidex ];
  # systemd.user.services.kidex = {
  #   Unit = {
  #     Description = "Service for kidex, a file indexer";
  #   };
  #   Service = {
  #     ExecStart = "${pkgs.kidex}/bin/kidex";
  #   };
  #   Install = {
  #     WantedBy = [ "graphical-session.target" ];
  #   };
  # };
  programs.anyrun = {
    enable = true;
    config = {
      plugins = with inputs.anyrun.packages.${pkgs.system}; [
        # An array of all the plugins you want, which either can be paths to the .so files, or their packages
        applications
        # dictionary # Unnecessary
        # kidex # Needs kidex https://gnumbers.between 0 1ithub.com/Kirottu/kidex, which is not in nixpkgs yet. Made avialable through local package and an overlay. 
        # TODO when anyrun tries to use kidex it fails with error
        # Error reading directory /usr/share: No such file or directory (os error 2)
        # Error reading directory /nix/store/5pd54bk7gri5skzir87bxrws5g3qnhfh-desktops/share: No such file or directory (os error 2)
        # Error reading directory /var/lib/flatpak/exports/share: No such file or directory (os error 2)
        # Error reading directory /home/dae/.nix-profile/share: No such file or directory (os error 2)
        # Error reading directory /nix/profile/share: No such file or directory (os error 2)
        # Error reading directory /home/dae/.local/state/nix/profile/share: No such file or directory (os error 2)
        # Error reading directory /nix/var/nix/profiles/default/share: No such file or directory (os error 2)
        # Failed to get kidex index: An IO error occurred: No such file or directory (os error 2)
        # Not sure of how to fix, disabling kidex for now
        randr
        rink # https://github.com/tiffany352/rink-r
        shell
        # stdin # Only intended to be used if dmenu like functionality is needed
        symbols
        # translate # Unnecessary
        # websearch # Unnecessary
      ];
      width = {
        fraction = 0.3;
      };
      # position = "top";
      # verticalOffset = { absolute = 0; };
      hideIcons = false;
      ignoreExclusiveZones = false;
      layer = "overlay";
      hidePluginInfo = false;
      closeOnClick = false;
      showResultsImmediately = false;
      maxEntries = null;
    };
    extraCss =
      let
        borderWidth = config.variables.border.width.narrow.px;
        borderRadius = config.variables.border.radius.px;
      in
      ''
        @define-color color_bg alpha(#${config.colorScheme.palette.base00}, ${config.variables.opacity.string});
        @define-color color_bg_dark alpha(#${config.colorScheme.palette.base00}, ${config.variables.opacity.string});
        @define-color color_border #${config.colorScheme.palette.base09};
        @define-color color_border_active #${config.colorScheme.palette.base09};
        @define-color color_text #${config.colorScheme.palette.base05};
        @define-color color_urgent #${config.colorScheme.palette.base09};

        * {
          border: ${borderWidth} solid transparent;
          border-radius: ${borderRadius};
          font-family: ${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif};
          font-size: 12pt;
        }

        #window,
        #match,
        #entry,
        #plugin,
        #main {
         background: transparent;
        }

        #entry {
          background: @color_bg;
          border: ${borderWidth} solid @color_border;
          margin: 0.3em;
          padding-top: 0em;
          padding-bottom: 0em;
          padding-left: 0.2em;
          padding-right: 0.2em;
        }

        list > #plugin {
          margin: 0.2em;
        }
        list > #plugin:first-child { margin-top: 0.4em; }
        list > #plugin:last-child { margin-bottom: 0.4em; }

        box#main {
          background: @color_bg;
          padding: 0.2em;
        }

        #match:selected {
          border: ${borderWidth} solid @color_border;
          color: @color_border;
        }

        #match:hover, #plugin:hover {
          background: @color_bg;
          border: ${borderWidth} solid @color_border;
        }

      '';

    extraConfigFiles = {
      "applications.ron".text = ''
        Config(
          prefix: "",
          // Also show the Desktop Actions defined in the desktop files, e.g. "New Window" from LibreWolf
          desktop_actions: false,
          max_entries: 5, 
          // The terminal used for running terminal based desktop entries, if left as `None` a static list of terminals is used
          // to determine what terminal to use.
          terminal: Some("${config.home.sessionVariables.TERMINAL}"),
        )
      '';
      # "kidex.ron".text = ''
      #   Config(
      #   prefix: ":file",
      #     max_entries: 5,
      #   )
      # '';
      "randr.ron".text = ''
        Config(
          prefix: ":dp",
          max_entries: 5, 
        )
      '';
      # Rink must adhere to the formats here https://github.com/tiffany352/rink-r
      "shell.ron".text = ''
        Config(
          prefix: ":sh",
          // Override the shell used to launch the command
          shell: ${pkgs.nushell}/bin/nu,
         )
      '';
      "symbols.ron".text = ''
        Config(
          // The prefix that the search needs to begin with to yield symbol results
          prefix: ":sym",
          // Custom user defined symbols to be included along the unicode symbols
          symbols: {
            // "name": "text to be copied"
          },
          max_entries: 10,
        )
      '';
    };
  };
}
