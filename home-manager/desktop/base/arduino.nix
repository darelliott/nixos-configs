{ pkgs, ... }:
{
  home.packages = with pkgs; [
    fritzing
    arduino # IDE V1, works
    # arduino-ide # IDE V2, Does not work
    arduino-cli # cli, works
  ];
  programs.nushell.shellAliases = {
    "ar" = "arduino-cli";
  };
}
