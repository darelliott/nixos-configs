{ ... }:
{
  imports = [
    ./helix-extras
    ./arduino.nix
    ./zathura.nix
    ./flatpak.nix
    ./hyprland.nix
    ./wezterm.nix
    ./onedrive.nix
    ./anyrun.nix
    ./wm-misc.nix
    ./gammastep.nix
    ./quick-emu.nix
    ./ironbar.nix
    ./mako.nix
    ./protonvpn.nix
    ./bluetooth.nix
    ./swayosd.nix
  ];
}
