{
  pkgs,
  config,
  osConfig,
  ...
}:
{
  xdg.dataFile = {
    # Give all flatpaks access to theming folders
    "global" = {
      enable = true;
      target = "./flatpak/overrides/global";
      text = ''
        [Context]
        filesystems=xdg-data/icons:ro;xdg-config/gtk-3.0:ro;xdg-config/gtk-4.0:ro;xdg-data/fonts:ro;xdg-data/icons:ro;xdg-data/themes:ro;

        [Environment]
        QT_QPA_PLATFORMTHEME=${config.qt.style.name}
        QT_STYLE_OVERRIDE=${config.qt.style.name}
        GTK_THEME=${config.colorScheme.slug}
      '';
    };
    # Force wayland for mozilla apps. Can maybe be removed for firefox, but no harm in staying for a while
    "org.mozilla.firefox" = {
      enable = true;
      target = "./flatpak/overrides/org.mozilla.firefox";
      text = ''
        [Environment]
        MOZ_ENABLE_WAYLAND=1
      '';
    };
    "org.mozilla.Thunderbird" = {
      enable = true;
      target = "./flatpak/overrides/org.mozilla.Thunderbird";
      text = ''
        [Environment]
        MOZ_ENABLE_WAYLAND=1
      '';
    };
  };
  systemd.user = {
    timers = {
      flatpak-manager = {
        Unit = {
          Description = "Timer to manage flatpaks";
        };
        Timer = {
          OnBootSec = "2min";
          OnUnitActiveSec = "1day";
          Unit = [ "flatpak-manager.service" ];
        };
        Install = {
          WantedBy = [ "timers.target" ];
        };
      };
    };
    services =
      let
        # package that contains all theming data aggregated, to be used in bindmounts
        aggregated = pkgs.buildEnv {
          name = "aggregated-personalisation";
          paths = osConfig.fonts.packages ++ [
            # See theming for why gtk.cursorTheme is used instead of home.cursorPointer
            # config.home.pointerCursor.package
            config.gtk.iconTheme.package
            config.gtk.cursorTheme.package
            # Creating a default empty icons theme that inherits desired icons is an elegant way of getting the correct icons used.
            # Curso and Icon themes are both actually icon themes
            (pkgs.writeTextFile {
              name = "index.theme";
              destination = "/share/icons/default/index.theme";
              # https://wiki.archlinux.org/title/Cursor_themes#XDG_specification
              text = ''
                [Icon Theme]
                Name=Default
                Comment=Default Cursor and Icon Theme
                Inherits=${config.gtk.cursorTheme.name},${config.gtk.iconTheme.name}
              '';
            })
            config.gtk.theme.package
            # Same as for icon themes, but for gtk themes
            (pkgs.writeTextFile {
              name = "index.theme";
              destination = "/share/themes/default/index.theme";
              # https://wiki.archlinux.org/title/Cursor_themes#XDG_specification
              text = ''
                [Desktop Entry]
                Type=X-GNOME-Metatheme
                Name=${config.gtk.theme.name}
                Comment=Auto generated theme from nix-colors
                Encoding=UTF-8

                [X-GNOME-Metatheme]
                GtkTheme=${config.gtk.theme.name}
                MetacityTheme=${config.gtk.theme.name}
                IconTheme=${config.gtk.iconTheme.name}
                CursorTheme=${config.gtk.cursorTheme.name}
                CursorSize=24
              '';
            })
            config.qt.style.package
          ];
          pathsToLink = [
            "/share/fonts"
            "/share/icons"
            "/share/themes"
          ];
        };
        shareMount = path: {
          Unit = {
            Description = "Mount ${path} to $XDG_DATA_HOME/${path}";
          };
          Service = {
            Type = "forking";
            ExecStart =
              let
                customisation-mount = pkgs.writeShellScriptBin "customisation-mount" ''
                  #!/usr/bin/env bash
                  ${pkgs.coreutils}/bin/mkdir -p ${config.xdg.dataHome}/${path}
                  ${pkgs.bindfs}/bin/bindfs -r --resolve-symlinks --no-allow-other ${aggregated}/share/${path} ${config.xdg.dataHome}/${path}
                '';
              in
              "${customisation-mount}/bin/customisation-mount";
          };
          Install = {
            # Only needed for theming
            WantedBy = [ "graphical-session.target" ];
          };
        };
      in
      {
        "home-dae-.local-share-fonts" = shareMount "fonts";
        "home-dae-.local-share-icons" = shareMount "icons";
        "home-dae-.local-share-themes" = shareMount "themes";
        flatpak-manager = {
          Unit = {
            Description = "Automatically install, update and clean flatpaks";
            # network-online.target does not seem to exist?
            # Requires = [ "network-online.target" ];
          };
          Service = {
            Type = "oneshot";
            ExecStart =
              let
                # Flatpak to be installed
                flatpaks = [
                  "ch.protonmail.protonmail-bridge"
                  "com.calibre_ebook.calibre"
                  "org.inkscape.Inkscape"
                  "org.gimp.GIMP"
                  "org.libreoffice.LibreOffice"
                  "org.mozilla.Thunderbird"
                  "org.mozilla.firefox"
                  "org.videolan.VLC"
                  "org.gnome.Loupe"
                  "com.github.KRTirtho.Spotube"
                  "org.torproject.torbrowser-launcher"
                  "org.signal.Signal"
                ];
                manage-flatpaks = pkgs.writeShellScriptBin "manage-flatpaks" ''
                  #!/usr/bin/env bash
                  ${pkgs.flatpak}/bin/flatpak remote-add flathub https://dl.flathub.org/repo/flathub.flatpakrepo --if-not-exists --user # Ensure that a user install of flathub exists
                  ${pkgs.flatpak}/bin/flatpak install \
                  ${builtins.concatStringsSep " " flatpaks} \
                  --noninteractive
                  ${pkgs.flatpak}/bin/flatpak update --noninteractive
                  ${pkgs.flatpak}/bin/flatpak remove --unused --noninteractive # Removes all flatpaks installed as dependancies of now removed flatpaks
                  ${pkgs.flatpak}/bin/flatpak remove --delete-data --noninteractive # Removes config data for all flatpaks no longer installed
                '';
              in
              "${manage-flatpaks}/bin/manage-flatpaks";
          };
        };
        proton-bridge = {
          Unit = {
            Description = "Launch proton-bridge";
            After = [
              "graphical-session-pre.target"
              "flatpak-manager.service"
            ];
          };
          Service = {
            ExecStart = "${pkgs.flatpak}/bin/flatpak run ch.protonmail.protonmail-bridge --no-window";
          };
          Install = {
            WantedBy = [ "graphical-session.target" ];
          };
        };
      };
  };
}
