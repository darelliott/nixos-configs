_: {
  # Night light by times
  services.gammastep = {
    enable = true;
    provider = "manual";
    dawnTime = "06:00-08:00";
    duskTime = "19:30-21:00";
    temperature = {
      day = 8000;
      night = 2000;
    };
    tray = true;
  };
}
