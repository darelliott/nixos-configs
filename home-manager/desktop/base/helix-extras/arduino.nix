{ pkgs, ... }:
{
  programs.helix = {
    extraPackages = with pkgs; [ ];
    languages = {
      language = [
        {
          name = "arduino";
          language-id = "arduino";
          language-servers = [
            # "clangd"
            # "ltex-ls"
          ];
          file-types = [ "ino" ];
          grammar = "cpp";
          scope = "source.ino";
          roots = [
            "flake.nix"
            "*.ino"
          ];
          auto-format = true;
          formatter.command = "clang-format";
        }
      ];
    };
  };
}
