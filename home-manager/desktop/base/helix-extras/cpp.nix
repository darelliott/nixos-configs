{ pkgs, ... }:
{
  programs.helix = {
    extraPackages = with pkgs; [ clang-tools ];
    languages = {
      language = [
        {
          name = "cpp";
          language-servers = [
            "clangd"
            "ltex-ls"
          ];
          file-types = [
            "cpp"
            "h"
          ];
          scope = "source.cpp";
          roots = [ "flake.nix" ];
          auto-format = true;
          formatter = {
            command = "clang-format";
          };
        }
      ];
      language-server = {
        clangd = {
          command = "clangd";
          config = { };
        };
        ltex-ls.config.ltex-ls = {
          enabled = [ "cpp" ];
        };
      };
    };
  };
}
