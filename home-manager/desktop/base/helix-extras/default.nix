{ ... }:
{
  imports = [
    ./cpp.nix
    ./arduino.nix
    ./latex.nix
    ./ltex-ls.nix
    ./markdown.nix
    ./python.nix
  ];
}
