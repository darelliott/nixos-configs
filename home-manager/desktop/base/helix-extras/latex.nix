{ pkgs, ... }:
{
  home.packages = with pkgs; [
    tectonic
    (texlive.combine { inherit (texlive) scheme-infraonly latexindent; })
  ];
  programs.helix = {
    extraPackages = with pkgs; [
      ltex-ls
      texlab
    ];
    languages = {
      language = [
        {
          name = "latex";
          language-servers = [
            "texlab"
            "ltex-ls"
          ];
          file-types = [ "tex" ];
          scope = "source.latex";
          roots = [ ];
          auto-format = true;
          formatter = {
            command = "latexindent -w -c=/dev/null";
          };
        }
      ];
      language-server = {
        texlab = {
          command = "texlab";
          config.texlab = {
            auxDirectory = "build";
            build = {
              forwardSearchAfter = false;
              onSave = false;
              executable = "tectonic";
              args = [
                "-X"
                # "compile"
                # "%f"
                # "--synctex"
                "build"
                "--keep-logs"
                "--keep-intermediates"
              ];
            };
            forwardSearch = {
              executable = "zathura";
              # args = [
              #   "--synctex-forward"
              #   "%l:1:%f"
              #   "%p"
              # ];
            };
            chktex = {
              onOpenAndSave = true;
              onEdit = true;
            };
            # bibtexFormatter = "latexindent";
            latexFromatter = "latexindent";
            latexindent.modifyLineBreaks = true;
          };
        };
      };
    };
  };
}
