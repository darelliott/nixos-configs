_: {
  programs.helix.languages.language-server = {
    ltex-ls = {
      command = "ltex-ls";
      config.ltex-ls = {
        # enabled = true;
        language = "en-GB";
        completionEnabled = "false";
        disabledRules = {
          "en" = [ "REP_PASSIVE_VOICE" ];
          "en-GB" = [ "REP_PASSIVE_VOICE" ];
        };
      };
    };
  };
}
