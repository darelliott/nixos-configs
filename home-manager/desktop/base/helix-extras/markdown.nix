{ pkgs, ... }:
{
  programs.helix = {
    extraPackages = with pkgs; [ ltex-ls ];
    languages = {
      language = [
        {
          name = "markdown";
          language-servers = [ "ltex-ls" ];
          file-types = [ "md" ];
          scope = "source.markdown";
          roots = [ ];
          auto-format = true;
        }
      ];
    };
  };
}
