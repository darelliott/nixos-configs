{ pkgs, ... }:
{
  programs.helix = {
    extraPackages = with pkgs; [ python311Packages.python-lsp-server ];
    languages = {
      language = [
        {
          name = "python";
          # language-servers = [
          # ];
          file-types = [ "py" ];
          scope = "source.py";
          roots = [ "flake.nix" ];
          auto-format = false;
          # formatter = {
          #   command = "";
          # };
        }
      ];
      language-server = { };
    };
  };
}
