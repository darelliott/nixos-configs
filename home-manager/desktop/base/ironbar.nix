{
  inputs,
  lib,
  config,
  pkgs,
  osConfig,
  ...
}:
{
  imports = [ inputs.ironbar.homeManagerModules.default ];
  # And configure. Options at https://github.com/JakeStanger/ironbar/blob/master/flake.nix
  # For tray
  home.packages = with pkgs; [
    libappindicator # For tray
  ];
  variables.bar.height.int = 16;
  programs.ironbar = {
    enable = true;
    systemd = true;
    # package = ironbar;
    # style = "";
    # features = [ ];
    config =
      let
        base = {
          name = "top-bar";
          position = "top";
          height = config.variables.bar.height.int;
          margin = {
            # Margins are in pixels
            top = 0;
            bottom = 0;
            left = 0;
            right = 0;
          };
          # It should pick it up by default, but it fails to
          icon_theme = config.gtk.iconTheme.name;
          ironvar_defaults = {
            capsLockOn = "0";
          };
          start = [
            {
              bar = [
                {
                  label = "";
                  name = "power-btn";
                  on_click = "popup:toggle";
                  type = "button";
                }
              ];
              class = "power-menu";
              popup = [
                {
                  orientation = "vertical";
                  type = "box";
                  widgets = [
                    {
                      label = "Power menu";
                      name = "header";
                      type = "label";
                    }
                    {
                      type = "box";
                      widgets = [
                        {
                          class = "power-btn";
                          label = "<span font-size='32pt'></span>";
                          on_click = "!systemctl poweroff";
                          type = "button";
                        }
                        {
                          class = "power-btn";
                          label = "<span font-size='32pt'></span>";
                          on_click = "!systemctl reboot";
                          type = "button";
                        }
                        {
                          class = "power-btn";
                          label = "<span font-size='32pt'>⏾</span>";
                          on_click = "!systemctl suspend";
                          type = "button";
                        }
                        {
                          class = "power-btn";
                          label = "<span font-size='32pt'></span>";
                          on_click = "!swaylock";
                          type = "button";
                        }
                      ];
                    }
                  ];
                }
              ];
              type = "custom";
            }
            {
              type = "workspaces";
              all_monitors = true;
            }
            {
              type = "sys_info";
              format = [
                " {cpu_percent}%"
                "🌡 {temp_c:coretemp-Package-id-0}°C"
                " {memory_percent}%"
                "🖴 {disk_percent:/}%"
                (lib.mkIf osConfig.networking.wireless.iwd.enable "↓ {net_down:wlan0} / ↑ {net_up:wlan0} Mb/s")
                (lib.mkIf (!osConfig.networking.wireless.iwd.enable) "↓ {net_down:enp3s0} / ↑ {net_up:enp3s0} Mb/s")
              ];
              interval = {
                cpu = 5;
                memory = 5;
                temps = 5;
                disks = 60;
                networks = 1;
              };
            }
          ];
          center = [ ];
          end = [
            { type = "tray"; }
            {
              type = "label";
              label = "Caps Lock 🔒";
              show_if = "#capsLockOn"; # variable
            }
            {
              type = "music";
              player_type = "mpris";
              format = "{title} / {artist}";
              truncate = {
                mode = "end";
                max_length = 20;
              };
              icons = {
                play = "";
                pause = "";
              };
            }
            { type = "clock"; }
            {
              type = "clipboard";
              max_items = 3;
              truncate = {
                mode = "end";
                length = 40;
              };
            }
          ];
        };
        battery = {
          end = [
            {
              type = "upower";
              format = "{percentage}%";
            }
          ];
        };
      in
      {
        monitors = {
          # optiplex3020
          "DP-2" = lib.mkMerge [ base ];
          # swift3
          "eDP-1" = lib.mkMerge [
            base
            battery
          ];
        }; # Gets converted to Json
      };
    style =
      let
        borderWidth = config.variables.border.width.narrow.px;
        borderRadius = config.variables.border.radius.px;
        outerGap = config.variables.gaps.outer.px;
      in
      ''
        @define-color color_bg alpha(#${config.colorScheme.palette.base00}, ${
          builtins.toString (config.variables.opacity.number / 2)
        });
        @define-color color_bg_dark alpha(#${config.colorScheme.palette.base00}, ${
          builtins.toString (config.variables.opacity.number / 2)
        });
        @define-color color_border #${config.colorScheme.palette.base09};
        @define-color color_border_active #${config.colorScheme.palette.base09};
        @define-color color_text #${config.colorScheme.palette.base05};
        @define-color color_urgent #${config.colorScheme.palette.base09};
        /* -- base styles -- */

        * {
          border: ${borderWidth} solid transparent;
        	border-radius: ${borderRadius};
        	border-image: none;
        	outline: none;
        	background-image: none;
        	background-color: @color_bg;
        	box-shadow: none;
        	text-shadow: none;
        	color: inherit;
          font-family: ${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif}, sans-serif;
          font-size: 12pt;
        }

        *:hover {
          border: ${borderWidth} solid @color_border;
        }

        #bar {
        	border-radius: 0px 0px ${borderRadius} ${borderRadius};
          background-color: transparent;
          border: none;
        }

        #bar #start {
          margin-left: ${outerGap};
          background-color: transparent;
          border: inherit;
        }

        #bar #center {
          background-color: transparent;
          border: inherit;
        }

        #bar #end {
          margin-right: ${outerGap};
          background-color: transparent;
          border: inherit;
        }

        .background {
            background-color: transparent;
        }

        .widget-container {
        	border-radius: 0px 0px ${borderRadius} ${borderRadius};
        }

        .popup {
            border: ${borderWidth} solid @color_border;
            padding: 1em;
        }

        .workspaces .item {
            padding-top: 0em;
            padding-bottom: 0em;
            padding-left: 0.2em;
            padding-right: 0.2em;
        }

        .workspaces .item.focused {
          border: ${borderWidth} solid @color_border;
        	color: @color_border;
        }

      '';
  };
  # Stop having to bother manually restarting systemd service
  home.activation = {
    reloadIronbar =
      let
        ironbar-conditional-reload = pkgs.writeShellScriptBin "ironbar-conditional-reload" ''
          #!/usr/bin/env bash
          if ${pkgs.systemdMinimal}/bin/systemctl is-active ironbar.service --user --quiet; then
          ${pkgs.ironbar}/bin/ironbar reload
          fi
        '';
      in
      "${ironbar-conditional-reload}/bin/ironbar-conditional-reload";
  };
}
