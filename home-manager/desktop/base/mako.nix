{
  config,
  osConfig,
  pkgs,
  ...
}:
{
  services.mako =
    let
      base00 = "#${config.variables.colorScheme.palette.base00}"; # Default Background
      # base01 = "#${config.variables.colorScheme.palette.base01}"; # Lighter Background (Used for status bars, line number and folding marks)
      # base02 = "#${config.variables.colorScheme.palette.base02}"; # Selection Background
      # base03 = "#${config.variables.colorScheme.palette.base03}"; # Comments, Invisibles, Line Highlighting
      # base04 = "#${config.variables.colorScheme.palette.base04}"; # Dark Foreground (Used for status bars)
      base05 = "#${config.variables.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators
      # base06 = "#${config.variables.colorScheme.palette.base06}"; # Light Foreground (Not often used)
      # base07 = "#${config.variables.colorScheme.palette.base07}"; # Light Background (Not often used)
      # base08 = "#${config.variables.colorScheme.palette.base08}"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
      base09 = "#${config.variables.colorScheme.palette.base09}"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url
      # base0A = "#${config.variables.colorScheme.palette.base0A}"; # Classes, Markup Bold, Search Text Background
      # base0B = "#${config.variables.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted
      # base0C = "#${config.variables.colorSvariables.colorSchemeWithOpacitycheme.palette.base0C}"; # Support, Regular Expressions, Escape Characters, Markup Quotes
      # base0D = "#${config.variables.colorScheme.palette.base0D}"; # Functions, Methods, Attribute IDs, Headings
      # base0E = "#${config.variables.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
      # base0F = "#${config.variables.colorScheme.palette.base0F}"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>
      var = config.variables;
    in
    {
      enable = true;
      maxVisible = 10;
      # sort= ;
      layer = "overlay";
      anchor = "top-right";
      font = "${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif} 12";
      backgroundColor = base00;
      textColor = base05;
      width = 400;
      height = 400;
      # To align with upper right edge of hyprland window
      margin = "${var.gaps.outer.string},${var.gaps.outer.string},${var.gaps.outer.string},${var.gaps.outer.string}";
      # Align with edge of ironbar and screen
      # margin = "0,0,0,${var.gaps.inner.string}";
      # padding= ;
      borderSize = var.border.width.narrow.int;
      borderColor = base09;
      borderRadius = var.border.radius.int;
      progressColor = base09;
      icons = true;
      # maxIconSize = 64;
      iconPath = "${config.xdg.dataHome}/icons/default:${config.gtk.iconTheme.package}/share/icons/${config.gtk.iconTheme.name}:${config.gtk.cursorTheme.package}/share/icons/${config.gtk.cursorTheme.name}:${pkgs.hicolor-icon-theme}/share/icons/hicolor";
      markup = true;
      actions = true;
      # format= ;
      defaultTimeout = 6000;
      # ignoreTimeout= ;
      # groupBy= ;  
    };
}
