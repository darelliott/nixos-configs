{ pkgs, ... }:
{
  # https://github.com/abraunegg/onedrive
  # Has to be run by hand initially to generate auth token
  # Could possibly have it as a sops secret, but best not to bother
  # command is onedrive --confdir=~/.config/onedrive-0 --synchronize --resync --download-only
  xdg.configFile = {
    "sync_list" = {
      target = "./onedrive-0/sync_list";
      text = ''
        # It is recommended to have exclusions before inclusions
        !/Apps
        !/workingDVDs
        /*
      '';
    };
    "config" = {
      target = "./onedrive-0/config";
      text = ''
        # Configuration for OneDrive Linux Client
        # This file contains the list of supported configuration fields
        # with their default values.
        # All values need to be enclosed in quotes
        # When changing a config option below, remove the '#' from the start of the line
        # For explanations of all config options below see docs/USAGE.md or the man page.
        #
        sync_dir = "~/OneDrive"
        # skip_file = "~*|.~*|*.tmp"
        # monitor_interval = "300"
        # skip_dir = ""
        # log_dir = "/var/log/onedrive/"
        # drive_id = ""
        # upload_only = "false"
        # check_nomount = "false"
        # check_nosync = "false"
        # download_only = "false"
        # disable_notifications = "false"
        # disable_upload_validation = "false"
        # enable_logging = "false"
        # force_http_11 = "false"
        # local_first = "false"
        # no_remote_delete = "false"
        # skip_symlinks = "false"
        # debug_https = "false"
        # skip_dotfiles = "false"
        # skip_size = "1000"
        # dry_run = "false"
        # min_notify_changes = "5"
        # monitor_log_frequency = "6"
        # monitor_fullscan_frequency = "12"
        # sync_root_files = "false"
        # classify_as_big_delete = "1000"
        # user_agent = ""
        # remove_source_files = "false"
        # skip_dir_strict_match = "false"
        # application_id = ""
        # resync = "false"
        # resync_auth = "false"
        # bypass_data_preservation = "false"
        # azure_ad_endpoint = ""
        # azure_tenant_id = "common"
        # sync_business_shared_folders = "false"
        # sync_dir_permissions = "700"
        # sync_file_permissions = "600"
        # rate_limit = "131072"
        # webhook_enabled = "false"
        # webhook_public_url = ""
        # webhook_listening_host = ""
        # webhook_listening_port = "8888"
        # webhook_expiration_interval = "86400"
        # webhook_renewal_interval = "43200"
        # space_reservation = "50"
        # display_running_config = "false"
        # read_only_auth_scope = "false"
        # cleanup_local_files = "false"
        # operation_timeout = "3600"
        # dns_timeout = "60"
        # connect_timeout = "10"
        # data_timeout = "600"
        # ip_protocol_version = "0"
      '';
    };
    "onedrive-launcher" = {
      target = "./onedrive-launcher";
      text = ''
        onedrive-0
      '';
    };
  };
  home.packages = with pkgs; [ onedrive ];

  systemd.user.services = {
    "onedrive@" = {
      Unit = {
        Description = "Onedrive sync service";
      };
      Service = {
        Type = "simple";
        ExecStart = ''
          ${pkgs.onedrive}/bin/onedrive --monitor --confdir=%h/.config/%i
        '';
        Restart = "on-failure";
        RestartSec = 3;
        RestartPreventExitStatus = 3;
      };
    };

    onedrive-launcher =
      let
        onedriveLauncher = pkgs.writeShellScriptBin "onedrive-launcher" ''
          # XDG_CONFIG_HOME is not recognized in the environment here.
          if [ -f $HOME/.config/onedrive-launcher ]
          then
            # Hopefully using underscore boundary helps locate variables
            for _onedrive_config_dirname_ in $(${pkgs.coreutils}/bin/cat $HOME/.config/onedrive-launcher | ${pkgs.gnugrep}/bin/grep -v '[ \t]*#' )
            do
              systemctl --user start onedrive@$_onedrive_config_dirname_
            done
          else
            systemctl --user start onedrive@onedrive
          fi
        '';
      in
      {
        Service = {
          Type = "oneshot";
          ExecStart = "${onedriveLauncher}/bin/onedrive-launcher";
        };
        Install = {
          WantedBy = [ "graphical-session.target" ];
        };
      };
  };
}
