{ pkgs, ... }:
{
  # https://github.com/ProtonVPN/proton-vpn-gtk-app
  home.packages = with pkgs; [
    libappindicator # Secretive dependancy of protonvpn-gui
    protonvpn-cli
    protonvpn-gui
  ];
}
