{ pkgs, ... }:
{
  # https://github.com/quickemu-project/quickemu
  # Setup
  # cd ~/vms
  # quickget windows 11

  home.packages = with pkgs; [
    quickemu
    socat
    spice-vdagent
  ];
}
