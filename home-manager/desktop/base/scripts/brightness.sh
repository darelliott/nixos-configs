#!/usr/bin/env bash
notify_brightness() {
  true
  echo target
  # notify-send --app-name="progress" --hint="string:wired-tag:brightness" --hint="int:value:$target" "Brightness" --hint="string:wired-note:font-icon" "🔆"
}
case "$1" in
increase)
  brightnessctl s +10%
  # target=$(($(brightnessctl g) * 100 / $(brightnessctl m)))
  # notify_brightness
  ;;

decrease)
  brightnessctl s 10%-
  # target=$(($(brightnessctl g) * 100 / $(brightnessctl m)))
  # notify_brightness
  ;;
esac
