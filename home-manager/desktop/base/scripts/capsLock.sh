#!/usr/bin/env bash
sleep 0.2 # Wihout sleep there are issues with race conditions between changing the brightness file and this script reading it.
if [[ $(cat /sys/class/leds/input0::capslock/brightness) == 1 ]]; then
  # notify-send --app-name="caps-lock" "Caps Lock On" --hint="string:wired-tag:caps-lock" --hint="string:wired-note:font-icon" "︎🔒"
  ironbar set capsLockOn 1
else
  # notify-send --app-name="caps-lock" "Caps Lock Off" --hint="string:wired-tag:caps-lock" --hint="string:wired-note:font-icon" "︎🔓"
  ironbar set capsLockOn 0
fi
