#!/usr/bin/env bash
notify_volume() {
  true
  # if [[ "$(wpctl get-volume @DEFAULT_AUDIO_SOURCE@)" == *\[MUTED\] ]]; then
  #   notify-send --app-name="microphone" --hint="string:wired-tag:microphone" "Microphone Off" --hint="string:wired-note:font-icon" "🎤"
  # else
  #   notify-send --app-name="microphone" --hint="string:wired-tag:microphone" "Microphone On" --hint="string:wired-note:font-icon" "🎤"
  # fi
}
case "$1" in
toggle)
  wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle
  notify_volume
  ;;
esac
