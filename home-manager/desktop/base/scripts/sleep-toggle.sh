#!/usr/bin/env bash
if [ -z "$id" ]; then
  id=0
fi
if systemctl is-active swayidle.service --user --quiet; then
  systemctl stop swayidle.service --user
  id=$(notify-send --app-name="lock" "Swayidle Off" -p -r "$id")
else
  systemctl start swayidle.service --user
  id=$(notify-send --app-name="lock" "Swayidle On" -p -r "$id")
fi
export id
