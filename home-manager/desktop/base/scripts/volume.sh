#!/usr/bin/env bash
notify_volume() {
  true
  # if [[ "$(wpctl get-volume @DEFAULT_AUDIO_SINK@)" == *\[MUTED\] ]]; then
  # notify-send --app-name="progress" --hint="string:wired-tag:volume" --hint="int:value:$volume" "Volume" --hint="string:wired-note:font-icon" "🔇"
  # else
  #   notify-send --app-name="progress" --hint="string:wired-tag:volume" --hint="int:value:$volume" "Volume" --hint="string:wired-note:font-icon" "🔈"
  # fi
}
case "$1" in
increase)
  wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%+
  # num=$(grep -o "\([0-9]\.[0-9]*\)" <<<"$(wpctl get-volume @DEFAULT_AUDIO_SINK@)")
  # percentage=$(echo "$num * 100" | bc)
  # volume=${percentage%%.*}
  notify_volume
  ;;
decrease)
  wpctl set-volume -l 1 @DEFAULT_AUDIO_SINK@ 5%-
  # num=$(grep -o "\([0-9]\.[0-9]*\)" <<<"$(wpctl get-volume @DEFAULT_AUDIO_SINK@)")
  # percentage=$(echo "$num * 100" | bc)
  # volume=${percentage%%.*}
  notify_volume
  ;;

toggle)
  wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
  # num=$(grep -o "\([0-9]\.[0-9]*\)" <<<"$(wpctl get-volume @DEFAULT_AUDIO_SINK@)")
  # percentage=$(echo "$num * 100" | bc)
  # volume=${percentage%%.*}
  notify_volume
  ;;
esac
