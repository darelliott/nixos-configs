#!/usr/bin/env bash
# see https://github.com/quickemu-project/quickemu
mkdir -p ~/vms
cd ~/vms || exit
quickemu --vm windows-11.conf --display spice
