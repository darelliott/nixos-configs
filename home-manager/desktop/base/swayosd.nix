{ pkgs, ... }:
{
  # services.swayosd = {
  #   enable = false;
  #   maxVolume = 100;
  # };
  home.packages = with pkgs; [ swayosd ];
  systemd.user.services = {
    # Fails due to being unable to own dbus interface
    # Must be ran as root
    # swayosd-libinput-backend = {
    #   Unit = {
    #     Description = "SwayOSD LibInput backend for listening to certain keys like CapsLock, ScrollLock, VolumeUp, etc...";
    #     Documentation = "https://github.com/ErikReider/SwayOSD";
    #     PartOf = [ "graphical.target" ];
    #     After = [ "graphical.target" ];
    #   };
    #   Service = {
    #     Type = "dbus";
    #     BusName = "org.erikreider.swayosd";
    #     ExecStart = "${pkgs.swayosd}/bin/swayosd-libinput-backend";
    #     Restart = "on-failure";
    #   };
    #   Install = {
    #     WantedBy = [ "graphical.target" ];
    #   };
    # };

    swayosd-server = {
      Unit = {
        Description = "Launch swayosd-server";
        After = [ "graphical-session-pre.target" ];
        # Requires = [ "swayosd-libinput-backend.service" ];
      };
      Service = {
        ExecStart = "${pkgs.swayosd}/bin/swayosd-server";
      };
      Install = {
        WantedBy = [ "hyprland-session.target" ];
      };
    };
  };
  wayland.windowManager.hyprland.settings.bind = [
    ",XF86MonBrightnessUp  , exec, ${pkgs.swayosd}/bin/swayosd-client --brightness raise"
    ",XF86MonBrightnessDown, exec, ${pkgs.swayosd}/bin/swayosd-client --brightness lower"
    ",XF86AudioRaiseVolume , exec, ${pkgs.swayosd}/bin/swayosd-client --output-volume raise"
    ",XF86AudioLowerVolume , exec, ${pkgs.swayosd}/bin/swayosd-client --output-volume lower"
    ",XF86AudioMute        , exec, ${pkgs.swayosd}/bin/swayosd-client --output-volume mute-toggle"
    ",XF86AudioMicMute     , exec, ${pkgs.swayosd}/bin/swayosd-client --input-volume mute-toggle"
    # ",Caps_Lock            , exec, ${pkgs.swayosd}/bin/swayosd-client --caps-lock"
    # system libinput backend handles caps lock, num lock, etc
  ];
}
