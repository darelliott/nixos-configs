{ config, osConfig, ... }:
{
  # https://wezfurlong.org/wezterm/index.html
  # Better image and unicode support than alacritty, faster than kitty
  home.sessionVariables = {
    TERMINAL = "wezterm";
    TERM = "wezterm";
  };
  programs.wezterm = {
    enable = true;
    colorSchemes = {
      nix-colors =
        let
          base00 = "#${config.colorScheme.palette.base00}"; # Default Background #Lighter Background (Used for status bars, line number and folding marks) #Selection Background
          base03 = "#${config.colorScheme.palette.base03}"; # Comments, Invisibles, Line Highlighting #Dark Foreground (Used for status bars)
          base05 = "#${config.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators #Light Foreground (Not often used)
          base07 = "#${config.colorScheme.palette.base07}"; # Light Background (Not often used)
          base08 = "#${config.colorScheme.palette.base08}"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted #Integers, Boolean, Constants, XML Attributes, Markup Link Url
          base0A = "#${config.colorScheme.palette.base0A}"; # Classes, Markup Bold, Search Text Background
          base0B = "#${config.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted
          base0C = "#${config.colorScheme.palette.base0C}"; # Support, Regular Expressions, Escape Characters, Markup Quotes
          base0D = "#${config.colorScheme.palette.base0D}"; # Functions, Methods, Attribute IDs, Headings
          base0E = "#${config.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed #Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>
        in
        {
          ansi = [
            base00
            base08
            base0B
            base0A
            base0D
            base0E
            base0C
            base05
          ];
          brights = [
            base03
            base08
            base0B
            base0A
            base0D
            base0E
            base0C
            base07
          ];
          background = base00;
          cursor_bg = base05;
          cursor_border = base05;
          cursor_fg = base05;
          foreground = base05;
          indexed = { };
          selection_bg = base05;
          selection_fg = base00;
        };
    };
    extraConfig = ''
      local wezterm = require 'wezterm'
      local config = {}
      config = {
        color_scheme = 'nix-colors',
        font = wezterm.font '${builtins.head osConfig.fonts.fontconfig.defaultFonts.monospace} 12',
        hide_tab_bar_if_only_one_tab = true,
        window_background_opacity = ${config.variables.opacity.string},
        default_prog = { 'nu' },
        enable_wayland = false -- TODO Added to fix incompatibilty with latest hyprland. Can be removed once updated wezterm released.
      }
      return config
    '';
  };
}
