{ config, pkgs, ... }:
{
  home.packages = with pkgs; [
    libsForQt5.polkit-kde-agent
    pavucontrol
    brightnessctl
    swaybg
    grim
    slurp
    xdg-user-dirs
    networkmanagerapplet
    libnotify
  ];

  programs.swaylock = {
    enable = true;
    settings = {
      color = "#${config.colorScheme.palette.base00}";
      bs-hl-color = "#${config.colorScheme.palette.base0F}";
      caps-lock-bs-hl-color = "#${config.colorScheme.palette.base0F}";
      caps-lock-key-hl-color = "#${config.colorScheme.palette.base0B}";
      key-hl-color = "#${config.colorScheme.palette.base0B}";
      inside-clear-color = "#${config.colorScheme.palette.base03}";
      inside-ver-color = "#${config.colorScheme.palette.base03}";
      inside-wrong-color = "#${config.colorScheme.palette.base08}";
      ring-wrong-color = "#${config.colorScheme.palette.base08}";
      ring-color = "#${config.colorScheme.palette.base0A}";
      ring-caps-lock-color = "#${config.colorScheme.palette.base0A}";
      ring-clear-color = "#${config.colorScheme.palette.base0C}";
      text-clear-color = "#${config.colorScheme.palette.base0C}";
      ring-ver-color = "#${config.colorScheme.palette.base0D}";
      text-color = "#${config.colorScheme.palette.base05}";
      text-caps-lock-color = "#${config.colorScheme.palette.base05}";
      text-ver-color = "#${config.colorScheme.palette.base05}";
      text-wrong-color = "#${config.colorScheme.palette.base05}";
      daemonize = true;
    };
  };
  services = {
    blueman-applet.enable = true;

    network-manager-applet.enable = true;
    swayidle = {
      enable = true;
      timeouts = [
        {
          timeout = 180;
          command = "${pkgs.swaylock}/bin/swaylock";
        }
        {
          timeout = 240;
          command = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";
          resumeCommand = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";
        }
        {
          timeout = 300;
          command = "${pkgs.systemdMinimal}/bin/systemctl suspend";
        }
      ];
      events = [
        {
          event = "before-sleep";
          command = "${pkgs.swaylock}/bin/swaylock";
        }
      ];
      systemdTarget = "hyprland-session.target";
    };
  };
  systemd.user.services = {
    swaybg = {
      Unit = {
        Description = "Launch Swaybg";
        After = "graphical-session-pre.target";
      };
      Service = {
        # ExecStart =
        #   let
        #     nix-colors-lib = inputs.nix-colors.lib.contrib { inherit pkgs; };
        #     wallpaper = nix-colors-lib.nixWallpaperFromScheme {
        #       scheme = config.colorScheme;
        #       width = 1920;
        #       height = 1080;
        #       logoScale = 5.0;
        #     };
        #   in
        #   # wallpaper = ./wallpapers/wallpaper.jpg;
        #   "${pkgs.swaybg}/bin/swaybg --image ${wallpaper}";
        ExecStart = "${pkgs.swaybg}/bin/swaybg --image ${./wallpapers/wallpaper.jpg}";
      };
      Install = {
        WantedBy = [ "hyprland-session.target" ];
      };
    };
  };
}
