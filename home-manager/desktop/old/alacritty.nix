{ config, ... }:
{
  home.sessionVariables = { };
  programs.alacritty = {
    enable = true;
    settings = {
      opacity = 1.0;
      font = {
        size = 12;
      };
      shell = {
        program = "nu";
      };
      colors =
        let
          base00 = "#${config.colorScheme.palette.base00}"; # Default Background # Lighter Background (Used for status bars, line number and folding marks) # Selection Background
          base03 = "#${config.colorScheme.palette.base03}"; # Comments, Invisibles, Line Highlighting # Dark Foreground (Used for status bars)
          base05 = "#${config.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators # Light Foreground (Not often used)
          base07 = "#${config.colorScheme.palette.base07}"; # Light Background (Not often used)
          base08 = "#${config.colorScheme.palette.base08}"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted # Integers, Boolean, Constants, XML Attributes, Markup Link Url
          base0A = "#${config.colorScheme.palette.base0A}"; # Classes, Markup Bold, Search Text Background
          base0B = "#${config.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted
          base0C = "#${config.colorScheme.palette.base0C}"; # Support, Regular Expressions, Escape Characters, Markup Quotes
          base0D = "#${config.colorScheme.palette.base0D}"; # Functions, Methods, Attribute IDs, Headings
          base0E = "#${config.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>    "attributes" = base09k
        in
        {
          # Default colors
          primary = {
            background = base00;
            foreground = base05;
          };
          # Colors the cursor will use if `custom_cursor_colors` is true
          cursor = {
            text = base00;
            cursor = base05;
          };
          # Normal colors
          normal = {
            black = base00;
            red = base08;
            green = base0B;
            yellow = base0A;
            blue = base0D;
            magenta = base0E;
            cyan = base0C;
            white = base05;
          };
          # Bright colors
          bright = {
            black = base03;
            red = base08;
            green = base0B;
            yellow = base0A;
            blue = base0D;
            magenta = base0E;
            cyan = base0C;
            white = base07;
          };
        };
    };
  };
}
