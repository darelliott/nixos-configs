{ config, ... }:
{
  programs.joshuto = {
    enable = true;
    settings = {
      numbered_command = false;

      use_trash = true;
      watch_files = true;
      xdg_open = true;
      xdg_open_fork = true;

      display = {
        # default; hsplit
        mode = "default";

        automatically_count_files = false;
        collapse_preview = true;
        # ratios for parent view (optional); current view and preview
        column_ratio = [
          2
          4
          4
        ];
        scroll_offset = 6;
        show_borders = true;
        show_hidden = true;
        show_icons = true;
        tilde_in_titlebar = true;
        # none; absolute; relative
        line_number_style = "none";

        sort = {
          # lexical; mtime; natural; size; ext
          sort_method = "natural";
          case_sensitive = false;
          directories_first = true;
          reverse = false;
        };
      };
      # [preview]
      # max_preview_size = 2097152                           # 2MB
      # preview_script = "~/.config/joshuto/preview_file.sh" # make sure it's marked as executable

      tab = {
        # inherit; home; root
        home_page = "home";
      };
    };
    # theme = '' '';
    # Bookmarks option not enabled in home-manager module
    # bookmarks = [
    #   {
    #     key = "r";
    #     path = "/";
    #   }
    #   {
    #     key = "e";
    #     path = "/etc";
    #   }
    #   {
    #     key = "h";
    #     path = "~/";
    #   }
    #   {
    #     key = "t";
    #     path = "~/Onedrive/tcdEngineering";
    #   }
    # ];
    mimetype = {
      class = {
        audio_default = [
          {
            command = "org.videolan.VLC";
            args = [ "--" ];
            fork = true;
            silent = true;
          }
        ];

        image_default = [
          {
            command = "org.kde.gwenview";
            args = [ "--" ];
            fork = true;
            silent = true;
          }
        ];

        video_default = [
          {
            command = "org.videolan.VLC";
            args = [ "--" ];
            fork = true;
            silent = true;
          }
        ];

        text_default = [
          { command = "hx"; }
          { command = "nano"; }
        ];

        reader_default = [
          {
            command = "zathura";
            fork = true;
            silent = true;
          }
        ];

        libreoffice_default = [
          {
            command = "org.libreoffice.LibreOffice";
            fork = true;
            silent = true;
          }
        ];
      };
      extension = {
        ## image formats
        avif.app_list = config.programs.joshuto.mimetype.class.image_default;
        bmp.app_list = config.programs.joshuto.mimetype.class.image_default;
        gif.app_list = config.programs.joshuto.mimetype.class.image_default;
        heic.app_list = config.programs.joshuto.mimetype.class.image_default;
        jpeg.app_list = config.programs.joshuto.mimetype.class.image_default;
        jpe.app_list = config.programs.joshuto.mimetype.class.image_default;
        jpg.app_list = config.programs.joshuto.mimetype.class.image_default;
        jxl.app_list = config.programs.joshuto.mimetype.class.image_default;
        pgm.app_list = config.programs.joshuto.mimetype.class.image_default;
        png.app_list = config.programs.joshuto.mimetype.class.image_default;
        ppm.app_list = config.programs.joshuto.mimetype.class.image_default;
        webp.app_list = config.programs.joshuto.mimetype.class.image_default;

        svg.app_list = [
          {
            command = "org.inkscape.Inkscape";
            fork = true;
            silent = true;
          }
        ];

        ## audio formats
        aac.app_list = config.programs.joshuto.mimetype.class.audio_default;
        ac3.app_list = config.programs.joshuto.mimetype.class.audio_default;
        aiff.app_list = config.programs.joshuto.mimetype.class.audio_default;
        ape.app_list = config.programs.joshuto.mimetype.class.audio_default;
        dts.app_list = config.programs.joshuto.mimetype.class.audio_default;
        flac.app_list = config.programs.joshuto.mimetype.class.audio_default;
        m4a.app_list = config.programs.joshuto.mimetype.class.audio_default;
        mp3.app_list = config.programs.joshuto.mimetype.class.audio_default;
        oga.app_list = config.programs.joshuto.mimetype.class.audio_default;
        ogg.app_list = config.programs.joshuto.mimetype.class.audio_default;
        opus.app_list = config.programs.joshuto.mimetype.class.audio_default;
        wav.app_list = config.programs.joshuto.mimetype.class.audio_default;
        wv.app_list = config.programs.joshuto.mimetype.class.audio_default;

        ## video formats
        avi.app_list = config.programs.joshuto.mimetype.class.video_default;
        av1.app_list = config.programs.joshuto.mimetype.class.video_default;
        flv.app_list = config.programs.joshuto.mimetype.class.video_default;
        mkv.app_list = config.programs.joshuto.mimetype.class.video_default;
        m4v.app_list = config.programs.joshuto.mimetype.class.video_default;
        mov.app_list = config.programs.joshuto.mimetype.class.video_default;
        mp4.app_list = config.programs.joshuto.mimetype.class.video_default;
        ts.app_list = config.programs.joshuto.mimetype.class.video_default;
        webm.app_list = config.programs.joshuto.mimetype.class.video_default;
        wmv.app_list = config.programs.joshuto.mimetype.class.video_default;

        ## text formats
        build.app_list = config.programs.joshuto.mimetype.class.text_default;
        c.app_list = config.programs.joshuto.mimetype.class.text_default;
        cmake.app_list = config.programs.joshuto.mimetype.class.text_default;
        conf.app_list = config.programs.joshuto.mimetype.class.text_default;
        cpp.app_list = config.programs.joshuto.mimetype.class.text_default;
        css.app_list = config.programs.joshuto.mimetype.class.text_default;
        csv.app_list = config.programs.joshuto.mimetype.class.text_default;
        cu.app_list = config.programs.joshuto.mimetype.class.text_default;
        ebuild.app_list = config.programs.joshuto.mimetype.class.text_default;
        eex.app_list = config.programs.joshuto.mimetype.class.text_default;
        env.app_list = config.programs.joshuto.mimetype.class.text_default;
        ex.app_list = config.programs.joshuto.mimetype.class.text_default;
        exs.app_list = config.programs.joshuto.mimetype.class.text_default;
        go.app_list = config.programs.joshuto.mimetype.class.text_default;
        h.app_list = config.programs.joshuto.mimetype.class.text_default;
        hpp.app_list = config.programs.joshuto.mimetype.class.text_default;
        hs.app_list = config.programs.joshuto.mimetype.class.text_default;
        html.app_list = config.programs.joshuto.mimetype.class.text_default;
        ini.app_list = config.programs.joshuto.mimetype.class.text_default;
        java.app_list = config.programs.joshuto.mimetype.class.text_default;
        js.app_list = config.programs.joshuto.mimetype.class.text_default;
        json.app_list = config.programs.joshuto.mimetype.class.text_default;
        kt.app_list = config.programs.joshuto.mimetype.class.text_default;
        lua.app_list = config.programs.joshuto.mimetype.class.text_default;
        log.app_list = config.programs.joshuto.mimetype.class.text_default;
        md.app_list = config.programs.joshuto.mimetype.class.text_default;
        micro.app_list = config.programs.joshuto.mimetype.class.text_default;
        nix.app_list = config.programs.joshuto.mimetype.class.text_default;
        flake.app_list = config.programs.joshuto.mimetype.class.text_default;
        lock.app_list = config.programs.joshuto.mimetype.class.text_default;
        inja.app_list = config.programs.joshuto.mimetype.class.text_default;
        py.app_list = config.programs.joshuto.mimetype.class.text_default;
        rkt.app_list = config.programs.joshuto.mimetype.class.text_default;
        rs.app_list = config.programs.joshuto.mimetype.class.text_default;
        scss.app_list = config.programs.joshuto.mimetype.class.text_default;
        sh.app_list = config.programs.joshuto.mimetype.class.text_default;
        srt.app_list = config.programs.joshuto.mimetype.class.text_default;
        svelte.app_list = config.programs.joshuto.mimetype.class.text_default;
        toml.app_list = config.programs.joshuto.mimetype.class.text_default;
        tsx.app_list = config.programs.joshuto.mimetype.class.text_default;
        txt.app_list = config.programs.joshuto.mimetype.class.text_default;
        vim.app_list = config.programs.joshuto.mimetype.class.text_default;
        xml.app_list = config.programs.joshuto.mimetype.class.text_default;
        yaml.app_list = config.programs.joshuto.mimetype.class.text_default;
        yml.app_list = config.programs.joshuto.mimetype.class.text_default;

        bz2.app_list = [
          {
            command = "tar";
            args = [ "-xvjf" ];
            confirm_exit = true;
          }
        ];
        gz.app_list = [
          {
            command = "tar";
            args = [ "-xvzf" ];
            confirm_exit = true;
          }
        ];
        tar.app_list = [
          {
            command = "tar";
            args = [ "-xvf" ];
            confirm_exit = true;
          }
        ];
        tgz.app_list = [
          {
            command = "tar";
            args = [ "-xvzf" ];
            confirm_exit = true;
          }
        ];
        xz.app_list = [
          {
            command = "tar";
            args = [ "-xvJf" ];
            confirm_exit = true;
          }
        ];

        m3u.app_list = [ { command = "hx"; } ];

        odt.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        odf.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        ods.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        odp.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;

        doc.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        docx.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        xls.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        xlsx.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        ppt.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;
        pptx.app_list = config.programs.joshuto.mimetype.class.libreoffice_default;

        pdf.app_list = config.programs.joshuto.mimetype.class.reader_default;

        tex.app_list = [ { command = "hx"; } ];
      };
      mimetype = {
        # application/octet-stream
        application.subtype.octet-stream.app_list = config.programs.joshuto.mimetype.class.video_default;

        # text/*
        text.app_list = config.programs.joshuto.mimetype.class.text_default;

        # text/*
        video.app_list = config.programs.joshuto.mimetype.class.video_default;
      };
    };
  };
  programs.nushell.shellAliases = {
    js = "joshuto";
  };
  xdg.configFile = {
    "bookmarks.toml" = {
      target = "./joshuto/bookmarks.toml";
      text = ''
        bookmark = [
              { key = "r", path = "/" },
              { key = "e", path = "/etc" },

              { key = "h", path = "~/" },
              { key = "t", path = "~/OneDrive/tcdEngineering" },
             ] '';
    };
  };
}
