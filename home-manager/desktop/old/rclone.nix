{ pkgs, config, ... }:
{
  home.packages = with pkgs; [
    rclone
    fuse
  ];
  # Config is handled as a sops secret, see sops.nix
  systemd.user.services.rclone-onedrive =
    let
      mountdir = "${config.home.homeDirectory}/onedrive-tcd";
    in
    {
      Unit = {
        Description = "Mount onedrive";
      };
      Install.WantedBy = [ "default.target" ];
      Service = {
        ExecStartPre = "/run/current-system/sw/bin/mkdir -p ${mountdir}";
        ExecStart = ''
          ${pkgs.rclone}/bin/rclone mount --vfs-cache-mode full onedrive: ${mountdir} --dir-cache-time 48h \
              --vfs-cache-max-age 48h \
              --vfs-read-chunk-size 10M \
              --vfs-read-chunk-size-limit 512M \
              # --config /home/dae/.config/rclone/rclone.conf \
              --buffer-size 512M
        '';
        ExecStop = "/run/current-system/sw/bin/fusermount -u ${mountdir}";
        Type = "notify";
        Restart = "always";
        RestartSec = "10s";
        Environment = [ "PATH=/run/wrappers/bin/:$PATH" ];
      };
    };
}
