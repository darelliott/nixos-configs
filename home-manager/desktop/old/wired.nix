{
  config,
  pkgs,
  inputs,
  osConfig,
  ...
}:
{
  # https://github.com/Toqozz/wired-notify
  imports = [ inputs.wired.homeManagerModules.default ];
  home.packages = with pkgs; [
    bc # math in bash
    libnotify
    playerctl
  ];
  services.wired = {
    enable = true;
    package = inputs.wired.packages.${pkgs.system}.default;
    config = pkgs.writeTextFile {
      name = "wired.ron";
      text =
        let
          base00 = "#${config.colorScheme.palette.base00}"; # Default Background # Lighter Background (Used for status bars, line number and folding marks) # Selection Background # Comments, Invisibles, Line Highlighting # Dark Foreground (Used for status bars)
          base05 = "#${config.colorScheme.palette.base05}"; # Default Foreground, Caret, Delimiters, Operators # Light Foreground (Not often used) # Light Background (Not often used) # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
          base09 = "#${config.colorScheme.palette.base09}"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url # Classes, Markup Bold, Search Text Background
          base0B = "#${config.colorScheme.palette.base0B}"; # Strings, Inherited Class, Markup Code, Diff Inserted # Support, Regular Expressions, Escape Characters, Markup Quotes # Functions, Methods, Attribute IDs, Headings
          base0E = "#${config.colorScheme.palette.base0E}"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
          base0F = "#${config.colorScheme.palette.base0F}"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>
          borderWidth = config.variables.border.width.wide.string;
          borderRadius = config.variables.border.radius.string;
          outerGap = config.variables.gaps.outer.int;
        in
        ''
          (
          	max_notifications: 10,
          	timeout: 3000,
          	poll_interval: 16,
          	shortcuts: (
          		notification_interact: 2,
          		notification_close: 1,
          		notification_closeall: 3,
          	),
          	history_length: 20,
          	replacing_resets_timeout: true,
          	min_window_width: 312,
          	layout_blocks: [
          		(
          			name: "root",
          			parent: "",
          			hook: (parent_anchor: TR, self_anchor: TR),
          			offset: (x: -${builtins.toString outerGap}, y: ${
               builtins.toString (3 * outerGap + config.variables.bar.height.int) # to ensure it aligns with top right of hyprland window
             }),
          			params: NotificationBlock((
          				monitor: 0,
          				border_width: ${borderWidth},
          				border_rounding: ${borderRadius},
          				gap: (x: 0.0, y: 24.0),
          				background_color: (hex: "${base00}"),
          				border_color: (hex: "${base0E}"),
          				border_color_low: (hex: "${base0F}"),
          				border_color_critical: (hex: "${base0B}"),
          				notification_hook: (parent_anchor: BL, self_anchor: TL),
          			)),
          		),
          		(
          			name: "summary",
          			parent: "root",
          			offset: (x: 0, y: 0),
          			hook: (parent_anchor: TR, self_anchor: TR),
          			params: TextBlock((
          				text: "%n : %s",
          				padding: (left: 8.0, right: 8.0, top: 4.0, bottom: 4.0),
          				font: "${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif} 14",
          				color: Color(hex: "${base05}"),
          				dimensions: (
          					width: (min: 312, max: 500),
          					height: (min: 0, max: 312),
          				),
                  dimensions_image_both: (
          					width: (min: 0, max: 300),
          					height: (min: 0, max: 300),
          				),
          			)),
          		),
          		(
          			name: "font-icon",
          			parent: "summary",
          			offset: (x: 0, y: 0),
          			hook: (parent_anchor: TL, self_anchor: TR),
          			render_criteria: [ Note("font-icon") ],
                render_anti_criteria: [ Or([HintImage,AppImage]) ],
          			params: TextBlock((
          				text: "%b",
          				padding: (left: 8.0, right: -4.0, top: 4.0, bottom: 4.0),
          				font: "${builtins.head osConfig.fonts.fontconfig.defaultFonts.emoji} 32",
          				color: Color(hex: "${base09}"),
          				dimensions: (
          					width: (min: 60, max: 300),
          					height: (min: 60, max: 300),
          				),
          			)),
          		),
          		(
          			name: "hint-image",
          			parent: "summary",
          			hook: (parent_anchor: TL, self_anchor: TR),
          			offset: (x: 0, y: 0),
          			render_criteria: [ HintImage ],
                render_anti_criteria: [ AppImage ],
          			params: ImageBlock((
          				image_type: Hint,
          				padding: (left: 8, right: -4.0, top: 4.0, bottom: 4.0),
          				rounding: 0.0,
          				scale_width: 60,
          				scale_height: 60,
          				filter_mode: Lanczos3,
          			)),
          		), 
          		(
          			name: "app-image",
          			parent: "summary",
          			hook: (parent_anchor: TL, self_anchor: TR),
          			offset: (x: 0, y: 0),
          			render_criteria: [ AppImage ],
                render_anti_criteria: [ HintImage ],
          			params: ImageBlock((
          				image_type: App,
          				padding: (left: 8, right: -4.0, top: 4.0, bottom: 4.0),
          				rounding: 0.0,
          				scale_width: 60,
          				scale_height: 60,
          				filter_mode: Lanczos3,
          			)),
          		), 
             	(
          			name: "body",
          			parent: "summary",
          			offset: (x: 0, y: 0),
          			hook: (parent_anchor: BL, self_anchor: TL),
          			render_criteria: [ Body ],
          			render_anti_criteria: [ Or([AppName("progress"),Note("font-icon")]) ],
          			params: ScrollingTextBlock((
          				text: "%b",
          				padding: (left: 8.0, right: 8.0, top: 0.0, bottom: 8.0),
          				font: "${builtins.head osConfig.fonts.fontconfig.defaultFonts.sansSerif} 12",
          				color: (hex: "${base05}"),
          				scroll_speed: 0.1,
          				lhs_dist: 24.0,
          				rhs_dist: 24.0,
          				scroll_t: 1.0,
          				width: (min: 500, max: 500),
          				width_image_both: (min: 172, max: 172),
          			)),
          		),
          		(
          			name: "progress",
          			parent: "body",
          			offset: (x: 0, y: 0),
          			hook: (parent_anchor: BL, self_anchor: TL),
          			render_criteria: [ Progress ],
          			params: ProgressBlock((
          				padding: (left: 8.0, right: 8.0, top: -4.0, bottom: 8.0),
          				border_width: 0.0,
          				border_rounding: 0.0,
          				fill_rounding: 0.0,
          				border_color: (hex: "${base09}"),
          				background_color: (hex: "${base00}"),
          				fill_color: (hex: "${base09}"),
          				width: 312.0,
          				height: 16.0,
          			)),
          		),
          	],
          )
        '';
    };
  };
}
