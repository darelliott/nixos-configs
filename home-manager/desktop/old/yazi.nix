{ pkgs, ... }:
{
  home.packages = with pkgs; [
    ffmpegthumbnailer
    unar
    jq
    poppler
    fd
    ripgrep
    fzf
    zoxide
  ];
  programs.yazi = {
    enable = true;
    # keymap = {
    # };
    enableNushellIntegration = true;
    settings = {
      manager = {
        # See https://yazi-rs.github.io/docs/usage/configuration/yazi for all options
        layout = [
          1
          4
          3
        ];
        sort_by = "alphabetical";
        sort_sensitive = false;
        sort_reverse = false;
        sort_dir_first = true;
        show_hidden = true;
        show_symlink = true;
      };
      preview = { };
      # Opener defaults to that defined by mime types, so need to define most openers
      # At least, I though it did. Seems not to be the case any more, hence hacky xdg-open method
      opener = {
        archive = [
          {
            exec = ''unar "$1"'';
            desc = "Extract here";
          }
        ];
        xdg-open = [
          {
            exec = "xdg-open $1";
            desc = "Extract here";
          }
        ];
      };
      open = {
        rules = [
          {
            mime = "*/*";
            use = "xdg-open";
          }
        ];
      };
    };
  };
}
