{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  imports = [
    inputs.sops-nix.nixosModules.sops
    ./configuration.nix
    ../../nixos/common/base
    {
      # Linux latest kernel zfs flagges as broken, so use stable version
      boot.kernelPackages = lib.mkForce pkgs.linuxPackages;
      users.users = {
        root = {
          initialPassword = lib.mkForce "password";
          password = lib.mkForce "password"; # For bootstrapping an installation before sops is configured
          initialHashedPassword = "";
          hashedPassword = null;
          hashedPasswordFile = null;
        };
        dae = {
          hashedPasswordFile = lib.mkForce null;
          password = lib.mkForce "password"; # For bootstrapping an installation before sops is configured
        };
      };
    }
    ../../nixos/desktop/base
    { boot.plymouth.enable = lib.mkForce false; }
    ../../nixos/desktop/optional/network-wifi.nix
    ../../nixos/desktop/optional/bluetooth.nix
    ../../secrets
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true; # makes hm use nixos's pkgs value
        useUserPackages = true;
        extraSpecialArgs = {
          inherit inputs;
        }; # allows access to flake inputs in hm modules
        users.dae.imports = [
          ../../home-manager/common/base
          ../../home-manager/desktop/base
        ];
      };
    }
  ];
}
