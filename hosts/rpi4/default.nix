{ inputs, ... }:
{
  imports = [
    inputs.sops-nix.nixosModules.sops
    inputs.nixos-hardware.nixosModules.raspberry-pi-4
    ./configuration.nix
    ./hardware-configuration.nix
    ../../nixos/common/base
    ../../nixos/server/base
    ../../nixos/server/optional
    ../../secrets
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true; # makes hm use nixos's pkgs value
        useUserPackages = true;
        extraSpecialArgs = {
          inherit inputs;
        }; # allows access to flake inputs in hm modules
        users.dae.imports = [ ../../home-manager/common/base ];
      };
    }
  ];
}
