{ inputs, ... }:
{
  imports = [
    inputs.sops-nix.nixosModules.sops
    inputs.disko.nixosModules.disko
    { _module.args.disk = "/dev/nvme0n1"; }
    ./configuration.nix
    ./hardware-configuration.nix
    ../../nixos/common/base
    ../../nixos/common/optional/power-management.nix
    ../../nixos/common/optional/disko-config-swap.nix
    ../../nixos/common/optional/impermanence.nix
    ../../nixos/common/optional/btrfs-wipe.nix
    ../../nixos/common/optional/secure-boot.nix
    ../../nixos/common/optional/tpm-boot.nix
    ../../nixos/desktop/base
    ../../nixos/desktop/optional/network-wifi.nix
    ../../nixos/desktop/optional/bluetooth.nix
    # ../../nixos/desktop/optional/plasma6.nix
    # ../../nixos/desktop/optional/cosmic.nix
    ../../nixos/desktop/optional/wireguard-client.nix
    # ../../nixos/server/optional/container-test.nix
    # ../../nixos/server/optional/ddns-updater.nix
    ../../secrets
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true; # makes hm use nixos's pkgs value
        useUserPackages = true;
        extraSpecialArgs = {
          inherit inputs;
        }; # allows access to flake inputs in hm modules
        users.dae.imports = [
          ../../home-manager/common/base
          ../../home-manager/desktop/base
        ];
      };
    }
  ];
}
