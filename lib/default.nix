{ inputs, ... }:
let
  helpers = import ./helpers.nix { inherit inputs; };
in
{
  inherit (helpers) systems;
  inherit (helpers) pkgsFor;
  inherit (helpers) forEachSystem;
}
