{ inputs, ... }:
rec {
  systems = [
    "x86_64-linux"
    "aarch64-linux"
  ];
  pkgsFor = inputs.nixpkgs.lib.genAttrs systems (system: import inputs.nixpkgs { inherit system; });
  forEachSystem = f: inputs.nixpkgs.lib.genAttrs systems (system: f pkgsFor.${system});
}
