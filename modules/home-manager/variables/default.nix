{ config, lib, ... }:

let
  cfg = config.variables;
in
{
  options.variables = {
    opacity = {
      number = lib.mkOption {
        type = lib.types.numbers.between 0 1;
        default = 1;
        description = "Opacity to be used where possible. Number between 0 and 1";
      };
      string = lib.mkOption {
        type = lib.types.str;
        default = builtins.toString cfg.opacity.number;
        description = "Opacity to be used where possible. Number between 0 and 1. As a string";
      };
      hex = lib.mkOption {
        type = lib.types.str;
        default = "FF";
        description = "Opacity to be used where possible. Number between 0 and 1. As hex";
      };
    };
    colorScheme = lib.mkOption {
      type = lib.types.attrs;
      default = {
        slug = "${config.colorScheme.slug}Custom";
        name = "${config.colorScheme.name}Custom";
        inherit (config.colorScheme) author;
        palette = {
          base00 = "${config.colorScheme.palette.base00}${cfg.opacity.hex}"; # Default Background
          base01 = "${config.colorScheme.palette.base01}FF"; # Lighter Background (Used for status bars, line number and folding marks)
          base02 = "${config.colorScheme.palette.base02}FF"; # Selection Background
          base03 = "${config.colorScheme.palette.base03}FF"; # Comments, Invisibles, Line Highlighting
          base04 = "${config.colorScheme.palette.base04}FF"; # Dark Foreground (Used for status bars)
          base05 = "${config.colorScheme.palette.base05}FF"; # Default Foreground, Caret, Delimiters, Operators
          base06 = "${config.colorScheme.palette.base06}FF"; # Light Foreground (Not often used)
          base07 = "${config.colorScheme.palette.base07}${cfg.opacity.hex}"; # Light Background (Not often used)
          base08 = "${config.colorScheme.palette.base08}FF"; # Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
          base09 = "${config.colorScheme.palette.base09}FF"; # Integers, Boolean, Constants, XML Attributes, Markup Link Url
          base0A = "${config.colorScheme.palette.base0A}FF"; # Classes, Markup Bold, Search Text Background
          base0B = "${config.colorScheme.palette.base0B}FF"; # Strings, Inherited Class, Markup Code, Diff Inserted
          base0C = "${config.colorScheme.palette.base0C}FF"; # Support, Regular Expressions, Escape Characters, Markup Quotes
          base0D = "${config.colorScheme.palette.base0D}FF"; # Functions, Methods, Attribute IDs, Headings
          base0E = "${config.colorScheme.palette.base0E}FF"; # Keywords, Storage, Selector, Markup Italic, Diff Changed
          base0F = "${config.colorScheme.palette.base0F}FF"; # Deprecated, Opening/Closing Embedded Language Tags, e.g. <?php ?>
        };
      };
      description = "Color shceme in hexcode, with opacity.";
    };
    border = {
      radius = {
        int = lib.mkOption {
          type = lib.types.int;
          default = 8;
          description = ''
            Default rounding radius of borders.
          '';
        };
        string = lib.mkOption {
          type = lib.types.str;
          default = builtins.toString cfg.border.radius.int;
        };
        px = lib.mkOption {
          type = lib.types.str;
          default = cfg.border.radius.string + "px";
        };
      };
      width = {
        wide = {
          int = lib.mkOption {
            type = lib.types.int;
            default = 2;
            description = ''
              Width of border.
            '';
          };
          string = lib.mkOption {
            type = lib.types.str;
            default = builtins.toString cfg.border.width.wide.int;
          };
          px = lib.mkOption {
            type = lib.types.str;
            default = cfg.border.width.wide.string + "px";
          };
        };
        narrow = {
          int = lib.mkOption {
            type = lib.types.int;
            default = cfg.border.width.wide.int / 2;
            description = ''
              Width of border.
            '';
          };
          string = lib.mkOption {
            type = lib.types.str;
            default = builtins.toString cfg.border.width.narrow.int;
          };
          px = lib.mkOption {
            type = lib.types.str;
            default = cfg.border.width.narrow.string + "px";
          };
        };
      };
    };
    gaps = {
      inner = {
        int = lib.mkOption {
          type = lib.types.int;
          default = 4;
          description = ''
            Spacing between interior windows.
          '';
        };
        string = lib.mkOption {
          type = lib.types.str;
          default = builtins.toString cfg.gaps.inner.int;
        };
        px = lib.mkOption {
          type = lib.types.str;
          default = cfg.gaps.inner.string + "px";
        };
      };
      outer = {
        int = lib.mkOption {
          type = lib.types.int;
          default = 8;
          description = ''
            Spacing between windows and monitor edge
          '';
        };
        string = lib.mkOption {
          type = lib.types.str;
          default = builtins.toString cfg.gaps.outer.int;
        };
        px = lib.mkOption {
          type = lib.types.str;
          default = cfg.gaps.outer.string + "px";
        };
      };
    };
    bar.height = {
      int = lib.mkOption {
        type = lib.types.int;
        default = 10;
        description = ''
          Thickness of top bar
        '';
      };
      string = lib.mkOption {
        type = lib.types.str;
        default = builtins.toString cfg.bar.height.int;
      };
      px = lib.mkOption {
        type = lib.types.str;
        default = cfg.bar.height.string + "px";
      };
    };
  };
  config = { };
}
