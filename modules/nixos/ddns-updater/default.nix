{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.services.ddns-updater;
in
{
  options.services.ddns-updater = {
    enable = lib.mkEnableOption "Container to update DNS records periodically with WebUI for many DNS providers";

    package = lib.mkOption {
      type = lib.types.package;
      default = pkgs.ddns-updater;
      description = ''
        The package to use for the ddns-updater service.
      '';
    };

    environmentalVariables = lib.mkOption {
      type = lib.types.attrsOf lib.types.str;
      description = "Environment varaibles to be set for the environment of the program executable. For full list see https://github.com/qdm12/ddns-updater";
      default = {
        LISTENING_ADDRESS = ":8000";
        DATADIR = "/updater/data";
      };
    };

    user = lib.mkOption {
      type = lib.types.str;
      description = "User to run service as. Owns DATADIR";
      default = "ddns-updater";
    };
  };

  config = lib.mkIf cfg.enable {
    services.ddns-updater.environmentalVariables = {
      LISTENING_ADDRESS = lib.mkDefault ":8000";
      DATADIR = lib.mkDefault "/updater/data";
    };

    systemd.services.ddns-updater =
      let
        envVars = lib.attrsets.mapAttrsToList (name: value: name + "=" + value) cfg.environmentalVariables;
      in
      {
        description = "DDNS-updater service";
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];

        serviceConfig = {
          TimeoutSec = "5min";
          Environment = envVars;
          ExecStart = "${cfg.package}/bin/ddns-updater";
          Restart = "always";
          User = "${cfg.user}";
          WorkingDirectory = cfg.environmentalVariables.DATADIR;
        };
      };

    users.users.${cfg.user} = {
      description = "DDNS Updater service user";
      home = cfg.environmentalVariables.DATADIR;
      createHome = true;
      isSystemUser = true;
      group = "${cfg.user}";
    };
    users.groups.${cfg.user} = { };

    systemd.tmpfiles.rules = [
      "d ${cfg.environmentalVariables.DATADIR} 0700 ${cfg.user} - - -"
      "Z ${cfg.environmentalVariables.DATADIR} 0700 ${cfg.user} - - -"
    ];
  };
}
