{ pkgs, lib, ... }:
{
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    loader = {
      efi.canTouchEfiVariables = true;
      grub.enable = lib.mkDefault false;
      generic-extlinux-compatible = {
        enable = lib.mkDefault false;
        configurationLimit = 10;
      };
      systemd-boot = {
        enable = lib.mkDefault true;
        configurationLimit = 10;
        editor = false;
      };
    };
    initrd = {
      systemd = {
        enable = lib.mkDefault true;
        # emergencyAccess = lib.mkDefault true;
      };
      supportedFilesystems = [ "btrfs" ];
    };
    swraid.enable = lib.mkDefault false;
  };
  services.getty = {
    greetingLine = ''
      Hello!
      If lost please return to:
      ----------------------------------
      | Name        : Darragh Elliott  |
      | Phone Number: +353857423812    |
      | Email       : me@delliott.xyz  |
      ----------------------------------
    '';
  };
}
