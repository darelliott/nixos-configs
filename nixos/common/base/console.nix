{ pkgs, config, ... }:
{
  console = {
    # See the folloing error in the log, but font is applied correctly.
    # systemd-vconsole-setup[180]: setfont: ERROR kdfontop.c:183 put_font_kdfontop: Unable to load such font with such kernel version
    # systemd-vconsole-setup[177]: /nix/store/qp6kq2m0g4ml94jrb6kb0yy4x1z8wj35-kbd-2.6.3/bin/setfont failed with exit status 71.
    # systemd-vconsole-setup[177]: Setting fonts failed with a "system error", ignoring.
    # Likely caused by systemd trying to apply font before vconsole is ready. It then retries later and succeeds.
    # Not sure how to delay initial attempt until it works, simply to remove error from log.

    earlySetup = true;
    # Must reference full path to prevent issues with finding file in initrd
    font = "${pkgs.terminus_font}/share/consolefonts/ter-132n.psf.gz";
    # font = "ter-132n";
    packages = with pkgs; [ terminus_font ];
    keyMap = "ie";
    colors = [
      "${config.home-manager.users.dae.colorScheme.palette.base00}"
      "${config.home-manager.users.dae.colorScheme.palette.base01}"
      "${config.home-manager.users.dae.colorScheme.palette.base02}"
      "${config.home-manager.users.dae.colorScheme.palette.base03}"
      "${config.home-manager.users.dae.colorScheme.palette.base04}"
      "${config.home-manager.users.dae.colorScheme.palette.base05}"
      "${config.home-manager.users.dae.colorScheme.palette.base06}"
      "${config.home-manager.users.dae.colorScheme.palette.base07}"
      "${config.home-manager.users.dae.colorScheme.palette.base08}"
      "${config.home-manager.users.dae.colorScheme.palette.base09}"
      "${config.home-manager.users.dae.colorScheme.palette.base0A}"
      "${config.home-manager.users.dae.colorScheme.palette.base0B}"
      "${config.home-manager.users.dae.colorScheme.palette.base0C}"
      "${config.home-manager.users.dae.colorScheme.palette.base0D}"
      "${config.home-manager.users.dae.colorScheme.palette.base0E}"
      "${config.home-manager.users.dae.colorScheme.palette.base0F}"
    ];
  };
}
