{ ... }:
{
  imports = [
    ./locale.nix
    ./misc.nix
    ./console.nix
    ./boot.nix
    ./nix.nix
    ./sudo.nix
    ./users.nix
    ./network-base.nix
  ];
}
