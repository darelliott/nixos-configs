{ pkgs, ... }:
{
  # List packages installed in system profile. To search, run:
  environment = {
    systemPackages = with pkgs; [
      helix
      git
    ];

    variables = {
      VISUAL = "hx";
      EDITOR = "hx";
    };

    defaultPackages = with pkgs; [ rsync ];
  };

  systemd.tmpfiles.rules = [ "Z /etc/nixos 0755 dae users - -" ];
  boot.tmp.cleanOnBoot = true;

  # Enable the OpenSSH daemon.
  services = {
    openssh = {
      enable = true;
      # hostKeys = [ ];
    };
    fwupd.enable = true;
    dbus = {
      enable = true;
      implementation = "broker";
    };
    # Clam AV antivirus
    # clamav = {
    #   daemon.enable = true;
    #   updater.enable = true;
    # };
  };

  programs = {
    ssh.startAgent = true;
  };
}
