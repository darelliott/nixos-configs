{
  config,
  lib,
  pkgs,
  ...
}:
{
  # see https://wiki.archlinux.org/title/Systemd-resolved
  services.resolved = {
    enable = lib.mkDefault true;
    # domains = [ "~." ]; # "use as default interface for all requests" # Unfortuately university wifi does not seem to play nice with attempting to route all traffic through preferred dns servers. 
    fallbackDns = config.networking.nameservers;
    # see https://man.archlinux.org/man/resolved.conf.5#OPTIONS
    llmnr = "true";
    dnssec = "allow-downgrade";
    dnsovertls = "opportunistic";
    extraConfig = lib.mkDefault ''
      MulticastDNS=resolve
    '';
  };
  networking = {
    dhcpcd.enable = lib.mkDefault false;
    wireless.enable = lib.mkDefault false;
    nameservers = [
      # Quad9 with default setup 
      # https://www.quad9.net/service/service-addresses-and-features
      "9.9.9.9"
      "149.112.112.112"
      "2620:fe::fe"
      "2620:fe::9"
    ];
    networkmanager = {
      enable = lib.mkDefault true;
      dns = "systemd-resolved";
    };
    nftables.enable = lib.mkDefault true;
    firewall = {
      enable = lib.mkDefault true;
      package = lib.mkDefault pkgs.nftables;
      allowedTCPPorts = [
        5355 # for LLMNR
      ];
      allowedUDPPorts = [
        5355 # for LLMNR
        5353 # For mDNS
      ];
    };
  };
}
