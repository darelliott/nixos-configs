{
  inputs,
  lib,
  pkgs,
  ...
}:
{
  # Enable flakes and better nix commands
  nix = {
    channel.enable = false;
    settings = {
      auto-optimise-store = true;
      allowed-users = [ "@wheel" ];
      auto-allocate-uids = true;
      keep-derivations = false;
      keep-outputs = false;
      use-cgroups = true;
      sandbox = true;
      use-xdg-base-directories = true;
      substituters = lib.mkForce [
        "https://cache.nixos.org/"
        "https://cache.ngi0.nixos.org/"
        "https://nix-community.cachix.org"
      ];
      trusted-public-keys = lib.mkForce [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "cache.ngi0.nixos.org-1:KqH5CBLNSyX184S9BKZJo1LxrxJ9ltnY2uAs5c/f1MA="
        "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      ];
      experimental-features = [
        "nix-command"
        "flakes"
        "auto-allocate-uids"
        "ca-derivations"
        "cgroups"
        # "git-hashing"
        "no-url-literals"
        # "parse-toml-timestamps"
        "read-only-local-store"
        # "recursive-nix"
        # "repl-flake"
        # "verified-fetches"
      ];
    };
    registry = {
      templates.flake = inputs.self;
      nixpkgs.flake = inputs.nixpkgs;
      ddns-updater-pull.flake = inputs.ddns-updater-pull;
    };
    gc = {
      automatic = true;
      dates = "weekly";
    };

    # Disable global registry and hence ensure maximum usage of local flake definitions and reduce system contact with internet
    extraOptions = ''
      flake-registry = 
    '';
  };
  # Emulate other versions of linux I expect to deploy configs on.
  boot.binfmt.emulatedSystems = builtins.filter (item: item != pkgs.system) inputs.self.libx.systems;
  nixpkgs = {
    overlays = [
      inputs.self.overlays.additions
      inputs.self.overlays.modifications
    ];
    config = {
      # allowAliases = false; # TODO Enabling this breaks the usage of system in the anyrun module. Would be nice to figure out how to fix that
      allowBroken = false;
      allowUnsupportedSystem = false;
      allowUnfree = false;
      # contentAddressedByDefault = true; # Causes too many things to be rebuilt, even with additional caches as above
    };
  };
}
