{ config, ... }:
{
  users.mutableUsers = false;
  users.users.dae = {
    isNormalUser = true;
    description = "dae";
    extraGroups = [
      "networkmanager"
      "wheel"
      "dialout"
      "video"
    ];
    hashedPasswordFile = config.sops.secrets.daeHashedPassword.path;
    # password = "password"; # For bootstrapping an installation before sops is configured
  };
}
