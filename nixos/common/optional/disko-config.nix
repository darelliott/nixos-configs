{
  disk ? "/dev/vda",
  ...
}:
{
  disko.devices = {
    disk = {
      vda = {
        type = "disk";
        device = disk;
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            {
              name = "ESP";
              start = "1MiB";
              end = "2000MiB";
              bootable = true;
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
                mountOptions = [ "defaults" ];
              };
            }
            {
              name = "luks";
              start = "2000MiB";
              end = "100%";
              content = {
                type = "luks";
                name = "crypted";
                extraOpenArgs = [ "--allow-discards" ];
                # if you want to use the key for interactive login be sure there is no trailing newline
                # for example use `echo -n "password" > /tmp/secret.key`
                # settings.keyFile = "/tmp/secret.key";
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ]; # Override existing partition
                  postCreateHook = ''
                    MNTPOINT=$(mktemp -d)
                    mount "/dev/mapper/crypted" "$MNTPOINT" -o subvol=/
                    trap 'umount $MNTPOINT; rm -rf $MNTPOINT' EXIT
                    btrfs subvolume snapshot -r $MNTPOINT/root $MNTPOINT/root-blank  
                  '';
                  subvolumes = {
                    "root" = {
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                      mountpoint = "/";
                    };
                    "home" = {
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                      mountpoint = "/home";
                    };
                    "nix" = {
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                      mountpoint = "/nix";
                    };
                    "persistent" = {
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                      mountpoint = "/persistent";
                    };
                    # "swap" = {
                    #   swap.swapfile.size = "10G";
                    #   mountpoint = "/swap";
                    # };
                  };
                };
              };
            }
          ];
        };
      };
    };
    # nodev."/" = {
    #   fsType = "tmpfs";
    #   mountOptions = [
    #     "size=3G"
    #     "defaults"
    #     "mode=755"
    #   ];
    # };
  };
  # fileSystems."/nix".neededForBoot = true;
  fileSystems."/persistent".neededForBoot = true;
  # fileSystems."/".neededForBoot = true;
  # Command to save a blank snapshot of root
  # btrfs subvolume snapshot -r /mnt/root /mnt/root-blank
}
