{
  inputs,
  config,
  lib,
  ...
}:
{
  imports = [ inputs.impermanence.nixosModules.impermanence ];
  # Hard link machine in initrd to prevent boot confusion issues. Runs in initrd, before impermanence normally runs.
  boot.initrd.systemd.services.persisted-files = {
    description = "Hard-link persisted files from /persist";
    wantedBy = [ "initrd.target" ];
    after = [ "sysroot.mount" ];
    unitConfig.DefaultDependencies = "no";
    serviceConfig.Type = "oneshot";
    script = ''
      mkdir -p /sysroot/etc/
      ln -snfT /persistent/etc/machine-id /sysroot/etc/machine-id
    '';
  };
  environment.persistence."/persistent" = {
    hideMounts = true;
    directories = [
      "/var/log"
      "/var/lib/nixos"
      "/var/lib/systemd"
      (lib.mkIf config.services.pipewire.alsa.enable "/var/lib/alsa")
      (lib.mkIf config.hardware.bluetooth.enable "/var/lib/bluetooth")
      (lib.mkIf config.networking.networkmanager.enable "/var/lib/NetworkManager")
      (lib.mkIf config.networking.wireless.iwd.enable "/var/lib/iwd")
      (lib.mkIf config.services.greetd.enable "/var/cache/tuigreet")

      (lib.mkIf config.networking.networkmanager.enable "/etc/NetworkManager/system-connections")
      "/etc/nixos"
      # Would make sense to use config.boot.lanzaboot.enable, but that modules is not loaded if not needed, eg on the raspi, so that causes errors.
      (lib.mkIf (config.networking.hostName != "rpi4") "/etc/secureboot")
      "/mnt/usb"
    ];
    files = [
      # "/etc/machine-id"
      "/etc/ssh/ssh_host_ed25519_key"
      "/etc/ssh/ssh_host_ed25519_key.pub"
      "/etc/ssh/ssh_host_rsa_key"
      "/etc/ssh/ssh_host_rsa_key.pub"
    ];
  };
}
