_: {
  powerManagement = {
    enable = true;
    powertop.enable = true;
  };
  services = {
    thermald.enable = true;

    auto-cpufreq = {
      enable = true;
      settings = {
        # settings for when connected to a power source
        charger = {
          # see available governors by running: cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors
          # preferred governor.
          governor = "performance";

          # minimum cpu frequency (in kHz)
          # example: for 800 MHz = 800000 kHz --> scaling_min_freq = 800000
          # see conversion info: https://www.rapidtables.com/convert/frequency/mhz-to-hz.html
          # to use this feature, uncomment the following line and set the value accordingly
          # scaling_min_freq = 800000

          # maximum cpu frequency (in kHz)
          # example: for 1GHz = 1000 MHz = 1000000 kHz -> scaling_max_freq = 1000000
          # see conversion info: https://www.rapidtables.com/convert/frequency/mhz-to-hz.html
          # to use this feature, uncomment the following line and set the value accordingly
          # scaling_max_freq = 1000000

          # turbo boost setting. possible values: always, auto, never
          turbo = "auto";
        };
        # settings for when using battery power
        battery = {
          # see available governors by running: cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors
          # preferred governor
          governor = "powersave";

          # minimum cpu frequency (in kHz)
          # example: for 800 MHz = 800000 kHz --> scaling_min_freq = 800000
          # see conversion info: https://www.rapidtables.com/convert/frequency/mhz-to-hz.html
          # to use this feature, uncomment the following line and set the value accordingly
          # scaling_min_freq = 800000

          # maximum cpu frequency (in kHz)
          # see conversion info: https://www.rapidtables.com/convert/frequency/mhz-to-hz.html
          # example: for 1GHz = 1000 MHz = 1000000 kHz -> scaling_max_freq = 1000000
          # to use this feature, uncomment the following line and set the value accordingly
          # scaling_max_freq = 1000000

          # turbo boost setting. possible values: always, auto, never
          turbo = "auto";
        };
      };
    };

    # TODO Attemps to fix weird suspending of mouse and keyboard with optiplex. kernelParams method a bit hamfisted and inefficient, but issues getting udev method to work

    # udev = {
    #   # enable = true;
    #   extraRules = ''
    #     # blacklist for usb autosuspend
    #     # Laptop Mouse
    #     # ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="17ef", ATTR{idProduct}=="6019", GOTO="power_usb_rules_end"
    #     ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="17ef", ATTR{idProduct}=="6019", ATTR{power/autosuspend}="-1"
    #     # Desktop mouse
    #     ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="1b1c", ATTR{idProduct}=="1b70", ATTR{power/autosuspend}="-1"

    #     # Desktop Keyboard
    #     ACTION=="add", SUBSYSTEM=="usb", ATTR{idVendor}=="1b1c", ATTR{idProduct}=="1b3d", ATTR{power/autosuspend}="-1"

    #     ACTION=="add", SUBSYSTEM=="usb", TEST=="power/control", ATTR{power/control}="auto"
    #     # LABEL="power_usb_rules_end"
    #   '';
    # };
  };
  # boot.initrd.services.udev.rules = config.services.udev.extraRules;
  boot.kernelParams = [ "usbcore.autosuspend=-1" ];
}
