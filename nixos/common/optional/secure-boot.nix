{
  inputs,
  pkgs,
  lib,
  ...
}:
{
  # See https://github.com/nix-community/lanzaboote/blob/master/docs/QUICK_START.md
  imports = [ inputs.lanzaboote.nixosModules.lanzaboote ];
  environment.systemPackages = [
    # For debugging and troubleshooting Secure Boot.
    pkgs.sbctl
  ];
  boot = {
    bootspec.enable = true;
    # Lanzaboote currently replaces the systemd-boot module.
    # This setting is usually set to true in configuration.nix
    # generated at installation time. So we force it to false
    # for now.
    loader.systemd-boot.enable = lib.mkForce false;

    lanzaboote = {
      enable = true;
      pkiBundle = "/etc/secureboot";
    };
  };
}
