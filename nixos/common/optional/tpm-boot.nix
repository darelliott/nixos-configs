_: {

  # TPM for unlocking LUKS
  #
  # TPM kernel module must be enabled for initrd. Device driver is viewable via the command:
  # sudo systemd-cryptenroll --tpm2-device=list
  # And added to a device's configuration:
  # boot.initrd.kernelModules = [ "tpm_tis" ];
  boot.initrd.kernelModules = [ "tpm_crb" ];

  # Must be enabled by hand - e.g.
  # sudo systemd-cryptenroll --wipe-slot=tpm2 /dev/nvme0n1p2 --tpm2-device=auto --tpm2-pcrs=0+2+7
  # See https://wiki.archlinux.org/title/Trusted_Platform_Module for more information
  security.tpm2.enable = true;
  security.tpm2.tctiEnvironment.enable = true;
}
