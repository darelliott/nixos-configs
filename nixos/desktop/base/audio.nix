{
  # Disabling other audio options
  sound.enable = false;
  hardware.pulseaudio.enable = false;
  services.jack.jackd.enable = false;

  # Enables pipewire to run in near realtime, reducing latency.
  # https://github.com/heftig/rtkit
  security.rtkit.enable = true;
  # Enable pipewire
  services.pipewire = {
    enable = true;
    audio.enable = true;
    wireplumber.enable = true;
    socketActivation = true;
    systemWide = false;
    alsa.enable = true;
    pulse.enable = true;
    jack.enable = false;
  };
  environment.etc = {
    "wireplumber/bluetooth.lua.d/51-bluez-config.lua".text = ''
      bluez_monitor.properties = {
        ["bluez5.enable-sbc-xq"] = true,
        ["bluez5.enable-msbc"] = true,
        ["bluez5.enable-hw-volume"] = true,
        ["bluez5.headset-roles"] = "[ hsp_hs hsp_ag hfp_hf hfp_ag ]"
      }
    '';
  };
}
