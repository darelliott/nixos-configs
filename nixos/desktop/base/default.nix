{ ... }:
{
  imports = [
    ./audio.nix
    ./xdg.nix
    ./printing.nix
    ./openrgb.nix
    ./window-manager.nix
    ./fonts.nix
    ./misc.nix
    ./greetd.nix
    ./flatpak.nix
    ./plymouth.nix
  ];
}
