{ pkgs, ... }:
{
  fonts = {
    packages = with pkgs; [
      (nerdfonts.override { fonts = [ "FiraCode" ]; })
      gyre-fonts
      dejavu_fonts
      noto-fonts
    ];

    fontconfig = {
      enable = true;
      defaultFonts = {
        serif = [ "FiraCode Nerd Font" ];
        sansSerif = [ "FiraCode Nerd Font" ];
        monospace = [ "FiraCode Nerd Font Mono" ];
        emoji = [ "Noto Color Emoji" ];
      };
      allowType1 = false;
      allowBitmaps = false;
      useEmbeddedBitmaps = false;
    };
    fontDir.enable = true;
  };
}
