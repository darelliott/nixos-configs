{
  pkgs,
  config,
  lib,
  ...
}:
{
  # Seeing the following journalctl log
  # greetd[1838]: pam_unix(greetd:auth): authentication failure; logname= uid=0 euid=0 tty= ruser= rhost=  user=dae
  # greetd[1838]: gkr-pam: unable to locate daemon control file
  # greetd[1838]: gkr-pam: stashed password to try later in open session
  # greetd[1838]: error: authentication error: pam_authenticate: AUTH_ERR
  # greetd[1508]: client loop failed: i/o error: Broken pipe (os error 32)
  # greetd[2252]: gkr-pam: unable to locate daemon control file
  # greetd[2252]: gkr-pam: stashed password to try later in open session
  # greetd[1589]: pam_unix(greetd:session): session closed for user greeter
  # Web searches indicate this is harmless, and caused by the password being entered before gnome keyring is loaded. 
  services.greetd = {
    enable = true;
    settings = {
      default_session = {
        command = ''
            ${pkgs.greetd.tuigreet}/bin/tuigreet --time \
            --remember \
            --greeting "
            Hello!
            If lost please return to:
            ----------------------------------
            | Name        : Darragh Elliott  |
            | Phone Number: +353857423812    |
            | Email       : me@delliott.xyz  |
            ----------------------------------
            " \
            --sessions ${config.services.xserver.displayManager.sessionData.desktops}/share/wayland-sessions \
          --cmd "${pkgs.hyprland}/bin/Hyprland &> /dev/null"
        '';
        user = "greeter";
      };
    };
  };
  systemd.services.greetd = {
    unitConfig = {
      After = lib.mkOverride 0 [ "multi-user.target" ];
    };
    serviceConfig = {
      Type = "idle";
    };
  };
  security.pam.services.greetd.enableGnomeKeyring = true;

  # environment.etc."greetd/environments".text = ''
  #   Hyprland
  #   bash
  #   nu
  # '';
  # environment.systemPackages = with pkgs; [ greetd.tuigreet ];
}
