{
  config,
  lib,
  pkgs,
  ...
}:
{
  hardware.i2c.enable = true;
  services.hardware.openrgb = {
    enable = true;
  };
  systemd.services.openrgb = {
    serviceConfig = {
      # ExecStart = let
      #   rgb = pkgs.writeShellScriptBin "rgb" ''
      #     #!/usr/bin/env bash
      #     NUM_DEVICES=$(${pkgs.openrgb}/bin/openrgb --noautoconnect --list-devices | grep -E '^[0-9]+: ' | wc -l)

      #     for i in $(seq 0 $(($NUM_DEVICES - 1))); do
      #       ${pkgs.openrgb}/bin/openrgb --noautoconnect --device $i --mode direct --color ${config.home-manager.users.dae.colorScheme.palette.base07} --server --server-port ${builtins.toString config.services.hardware.openrgb.server.port}
      #     done
      #   '';
      # in
      #   lib.mkForce "${rgb}/bin/rgb";
      # Port is 6742 by default
      ExecStart = lib.mkForce "${pkgs.openrgb}/bin/openrgb --noautoconnect --mode direct --color ${config.home-manager.users.dae.colorScheme.palette.base07} --server --server-port ${builtins.toString config.services.hardware.openrgb.server.port}";
    };
  };
}
