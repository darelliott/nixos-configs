{ pkgs, ... }:
{
  boot = {
    # Make boot quieter so it doesnt hide plymouth
    kernelParams = [
      "quiet"
      "splash"
      # "rd.systemd.show_status=false" # Seems to have no effect
      # "rd.udev.log_level=3" # Seems to have no effect
      # "udev.log_priority=3" # Seems to have no effect
      # "boot.shell_on_fail" # Requires root passowrd, and hence not useful right now.
    ];
    initrd.verbose = false;
    consoleLogLevel = 0;

    # TODO Having some issues after recent updates, plymouth not seen on boot, only on shutdown.
    # On boot instead see the OEM logo, and if quiet kernel disabled see systemd log, but never any plymouth.
    # Seems to be started and ended successfuly by systemd, but does not actually cover up the display?
    # Will leave for a time to see if if fixed in an update of if someone finds a fix online

    # Requires boot.initrd.systemd.enable = true to work properly when prompting for luks password
    plymouth =
      let
        theme = "spin";
      in
      {
        enable = true;
        # Some themes cause errors in plymouth, take care
        inherit theme;
        # logo = "";
        themePackages = [ (pkgs.adi1090x-plymouth-themes.override { selected_themes = [ theme ]; }) ];
        font = "${
          (pkgs.nerdfonts.override { fonts = [ "FiraCode" ]; })
        }/share/fonts/truetype/NerdFonts/FiraCodeNerdFont-Regular.ttf";
        # extraConfig = ''
        #   UseFirmwareBackground=false 
        #   DeviceScale=2 # Must be an integer
        # '';
      };
  };
  # This snippet should enable a smooth transition according to
  # https://wiki.archlinux.org/title/plymouth
  # While it does get the screen to show the splash screen, the splash stays on screen forever.
  # systemd.services.greetd = {
  #   unitConfig = {
  #     Conflicts = [ "plymouth-quit.service" ];
  #     After = [
  #       "plymouth-quit.service"
  #       "plymouth-start.service"
  #       "systemd-user-sessions.service"
  #     ];
  #     OnFailure = [ "plymouth-quit.service" ];
  #   };
  #   serviceConfig = {
  #     ExecStartPre = "${pkgs.plymouth}/bin/plymouth deactivate";
  #     ExecStartPost = [
  #       "sleep 30"
  #       "${pkgs.plymouth}/bin/plymouth quit --retain-splash"
  #     ];
  #   };
  # };
}
