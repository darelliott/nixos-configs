{ pkgs, lib, ... }:
{
  nixpkgs.config.allowUnfreePredicate =
    pkg:
    builtins.elem (lib.getName pkg) [
      # "hplipWithPlugin"
      # "hplip"
    ];
  services = {
    printing = {
      enable = true;
      drivers = with pkgs; [
        gutenprint # Drivers for many different printers from many different vendors.
        gutenprintBin # Additional, binary-only drivers for some printers.
        # hplip # Drivers for HP printers.
        # hplipWithPlugin # Drivers for HP printers, with the proprietary plugin. Use NIXPKGS_ALLOW_UNFREE=1 nix-shell -p hplipWithPlugin --run 'sudo -E hp-setup' to add the printer, regular CUPS UI doesn't seem to work.
        # postscript-lexmark # Postscript drivers for Lexmark
        # samsung-unified-linux-driver # Proprietary Samsung Drivers
        # splix # Drivers for printers supporting SPL (Samsung Printer Language).
        # brlaser # Drivers for some Brother printers
        # brgenml1lpr
        # brgenml1cupswrapper # Generic drivers for more Brother printers
        # cnijfilter2 # Drivers for some Canon Pixma devices (Proprietary driver)
      ];
    };
    # avahi = {
    #   enable = true;
    #   nssmdns4 = true;
    # };
  };
  environment.systemPackages = with pkgs; [
    system-config-printer
    # hplip
  ];
}
