{ pkgs, ... }:
{
  services.dbus.packages = with pkgs; [ swayosd ];
  systemd = {
    # packages = with pkgs; [ swayosd ];
    services.swayosd-libinput-backend = {
      unitConfig = {
        Description = "SwayOSD LibInput backend for listening to certain keys like CapsLock, ScrollLock, VolumeUp, etc...";
        Documentation = "https://github.com/ErikReider/SwayOSD";
        PartOf = [ "hyprland-session.target" ];
        After = [ "hyprland-session.target" ];
      };

      serviceConfig = {
        Type = "dbus";
        BusName = "org.erikreider.swayosd";
        ExecStart = "${pkgs.swayosd}/bin/swayosd-libinput-backend";
        Restart = "on-failure";
      };

      wantedBy = [ "graphical.target" ];
    };
  };

  programs.hyprland = {
    enable = true;
    xwayland.enable = true;
  };

  security.pam.services = {
    swaylock = { };
    dae.enableGnomeKeyring = true;
  };

  services = {
    gnome.gnome-keyring.enable = true;
    blueman.enable = true;
    upower.enable = true;
  };
}
