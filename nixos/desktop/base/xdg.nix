{ pkgs, ... }:
{
  xdg = {
    mime = {
      enable = true;
    };
    menus.enable = true;
    icons.enable = true;
    sounds.enable = true;
    autostart.enable = false;

    portal = {
      enable = true;
      xdgOpenUsePortal = true;
      extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    };
  };
}
