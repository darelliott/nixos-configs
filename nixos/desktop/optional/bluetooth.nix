_: {
  #Enable Bluetooth
  hardware.bluetooth = {
    enable = true;
    settings = {
      General = {
        Enable = "Source,Sink,Media,Socket";
      };
    };
    # Implemented by wireplumber, apparently?
    # hsphfpd.enable = true;
  };
  # See also the audio config file, as some settings interlock
}
