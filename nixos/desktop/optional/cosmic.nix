{ pkgs, ... }:
{
  # components that need to be in PATH
  environment.systemPackages = with pkgs; [
    cosmic-applibrary
    cosmic-applets
    cosmic-bg
    cosmic-comp
    cosmic-edit
    cosmic-files
    cosmic-greeter
    cosmic-icons
    cosmic-launcher
    cosmic-notifications
    cosmic-osd
    cosmic-panel
    cosmic-randr
    cosmic-screenshot
    cosmic-settings
    cosmic-settings-daemon
    cosmic-term
    cosmic-workspaces-epoch
    cosmic-session
  ];

  # COSMIC portal doesn't support everything yet
  xdg.portal.extraPortals = with pkgs; [
    xdg-desktop-portal-cosmic
    xdg-desktop-portal-gtk
  ];

  # session files for display manager and systemd
  services.xserver.displayManager.sessionPackages = with pkgs; [ cosmic-session ];
  systemd.packages = with pkgs; [ cosmic-session ];
}
