_: {
  networking = {
    wireless = {
      enable = false; # Disable wpa_supplicant
      dbusControlled = true;
      iwd = {
        enable = true;
        # https://iwd.wiki.kernel.org/networkconfigurationsettings
        settings = {
          General = {
            AddressRandomization = "network";
          };
          Network = {
            EnableIPv6 = true;
            NameResolvingService = "systemd";
          };
          Settings = {
            AutoConnect = true;
            AlwaysRandomizeAddress = true;
          };
        };
      };
    };
    networkmanager = {
      # Enabled in network base
      wifi = {
        backend = "iwd";
        macAddress = "random";
        scanRandMacAddress = true;
        # powersave = true; # Seems to cause frequent connection drops
      };
    };
  };
}
