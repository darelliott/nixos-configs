{ inputs, ... }:
{
  imports = [ inputs.kde2nix.nixosModules.plasma6 ];
  services.xserver.desktopManager.plasma6.enable = true;
}
