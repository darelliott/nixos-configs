{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ wireguard-tools ];
  networking.firewall = {
    # Apparently required to stop the firewall interfering with wireguard?
    # checkReversePath = false;
    allowedUDPPorts = [ 51820 ]; # Clients and peers can use the same port, see listenport
    # This might also allow clients to use wireguard properly?
    trustedInterfaces = [ "wg0" ];
  };
  # Config file is now placed using sops. See sops.nix in secrets
}
