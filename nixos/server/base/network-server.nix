{ config, pkgs, ... }:
{
  services.resolved = {
    enable = true;
    # Systemd resolved must be told not to run its dns stub on port 53, as it interferes with adguard home
    extraConfig = ''
      MulticastDNS=resolve
      DNSStubListener=no
    '';
  };
  systemd.network = {
    enable = true;
    networks = {
      "40-wired" = {
        enable = true;
        # see https://www.freedesktop.org/software/systemd/man/latest/systemd.network.html
        matchConfig.Type = "ether";
        # acquire a DHCP lease on link up
        networkConfig = {
          DHCP = "yes";
          Address = [ "192.168.0.111/24" ];
          DNS = config.networking.nameservers;
          IPv6AcceptRA = true;
        };
      };
    };
  };

  networking = {
    # Disable a bunch of stuff so it does not interfere with networkd
    dhcpcd.enable = false;
    useDHCP = false;
    useNetworkd = false;
    networkmanager.enable = false;
    # NAT is a kernel level feature it seems, and so can safely be enbaled and not conflict with systemd networkd
    nat = {
      enable = true;
      internalInterfaces = [ "ve-*" ];
      externalInterface = "eth0";
      enableIPv6 = true;
    };
    # Firewall, nftables is newer and better maintained than iptables
    nftables.enable = true;
    firewall = {
      enable = true;
      package = pkgs.nftables;
    };
  };
}
