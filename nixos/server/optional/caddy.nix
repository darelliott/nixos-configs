{ config, ... }:
{
  networking.firewall.allowedTCPPorts = [
    80
    443
  ];
  systemd.tmpfiles.rules = [
    "d /var/www 0755 ${config.services.caddy.user} ${config.services.caddy.user} - -"
    "Z /var/www 0755 ${config.services.caddy.user} ${config.services.caddy.user} - -"
  ];
  # TODO Use nixos containers
  services = {
    caddy = {
      enable = true;
      email = "me@delliott.xyz";
      virtualHosts."delliott.xyz".extraConfig = ''
        root * /var/www
        encode zstd
        file_server
      '';
    };
  };
}
