{ inputs, ... }:
{
  imports = [ inputs.self.nixosModules.ddns-updater ];
  # TODO Use nixos containers
  networking = {
    firewall = {
      allowedTCPPorts = [ 2000 ];
    };
  };
  services.ddns-updater = {
    enable = true;
    environmentalVariables = {
      LISTENING_ADDRESS = ":2000";
      DATADIR = "/var/ddns-updater";
    };
  };
}
