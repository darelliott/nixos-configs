{ ... }:
{
  imports = [
    ./ddns-updater.nix
    ./caddy.nix
    ./ssh.nix
    ./adguard.nix
    ./wireguard.nix
    # ./container-test.nix
  ];
}
