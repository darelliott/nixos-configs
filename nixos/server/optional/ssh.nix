{
  services.openssh = {
    enable = true;
    # require public key authentication for better security
    settings = {
      PasswordAuthentication = false;
      KbdInteractiveAuthentication = false;
      PermitRootLogin = "no";
    };
  };
  users.users."dae".openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIASe3AJ7nMDmYsJy2d8Mwd0dQBPYhFWCLePBClsI2keO dae@swift3"
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB2CpEOdJDynj6wQ1nxLI1zsOOSKDVYK24dYDNTrUuN7 dae@optiplex3020"
  ];
}
