{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [ wireguard-tools ];
  networking.firewall.allowedUDPPorts = [ 51820 ];

  systemd.network = {
    netdevs = {
      "50-wg0" = {
        netdevConfig = {
          Kind = "wireguard";
          Name = "wg0";
          MTUBytes = "1300";
        };
        wireguardConfig = {
          PrivateKeyFile = "/home/dae/.wireguard/private.key";
          ListenPort = 51820;
        };
        wireguardPeers = [
          {
            wireguardPeerConfig = {
              # Laptop
              # Public key of the peer (not a file path).
              PublicKey = "vB+WKnjhYV6BeHwVA2cEgZZOy9UtnX5xwdwDTb7tZT4=";
              # List of IPs assigned to this peer within the tunnel subnet. Used to configure routing.
              AllowedIPs = [
                "10.10.10.2/32"
                "fc10:10:10::2/128"
              ];
            };
          }
          {
            wireguardPeerConfig = {
              # Phone
              PublicKey = "JVUapXfe+5DxpR4vrAGjGKmreCelqQ2NH0ozKOxuXnU=";
              AllowedIPs = [
                "10.10.10.3/32"
                "fc10:10:10::3/128"
              ];
            };
          }
        ];
      };
    };
    networks.wg0 = {
      matchConfig.Name = "wg0";
      address = [
        "10.10.10.1/24"
        "fc10:10:10::1/64"
      ];
      networkConfig = {
        IPMasquerade = "both";
        IPForward = true;
      };
    };
  };
}
