{ inputs }:
{
  # This file defines overlays
  # This one brings our custom packages from the 'pkgs' directory
  additions = _final: _prev: inputs.self.packages;
  # This one contains whatever you want to overlay
  # You can change versions, add patches, set compilation flags, anything really.
  # https://nixos.wiki/wiki/Overlays
  modifications = _final: prev: {
    # example = prev.example.overrideAttrs (oldAttrs: rec {
    # ...
    # });
    processing = prev.processing.overrideAttrs (
      oldAttrs: rec {
        nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ prev.gsettings-desktop-schemas ];
        buildInputs = oldAttrs.buildInputs ++ [ prev.gsettings-desktop-schemas ];
        dontWrapGApps = false;
      }
    );
    inherit (inputs.ddns-updater-pull.legacyPackages.${prev.system}) ddns-updater;
    candy-icons = prev.candy-icons.overrideAttrs (
      oldAttrs: rec {
        propagatedBuildInputs = oldAttrs.nativeBuildInputs ++ [ prev.papirus-icon-theme ];
        installPhase = ''
          runHook preInstall

          mkdir -p $out/share/icons/candy-icons
          cp -a * $out/share/icons/candy-icons
          rm $out/share/icons/${oldAttrs.pname}/index.theme

          cat > $out/share/icons/${oldAttrs.pname}/index.theme << EOF
          [Icon Theme]
          Name=candy-icons
          Comment=Sweet gradient icons.
          Inherits=Papirus-Dark,breeze-dark,Adwaita,hicolor
          FollowsColorScheme=true

          # Directory list
          Directories=places/16,places/48,apps/scalable,devices/scalable,preferences/scalable,mimetypes/scalable

          [Apps]
          Context=Applications
          Size=96
          MinSize=8
          MaxSize=192
          Type=Scalable

          [apps/scalable]
          Size=96
          Context=Applications
          MinSize=8
          MaxSize=512
          Type=Scalable

          [devices/scalable]
          Size=96
          Context=Applications
          MinSize=8
          MaxSize=512
          Type=Scalable

          [preferences/scalable]
          Size=96
          Context=Applications
          MinSize=8
          MaxSize=512
          Type=Scalable

          [Mimes]
          Context=MimeTypes
          Size=96
          MinSize=8
          MaxSize=512
          Type=Scalable

          [mimetypes/scalable]
          Size=96
          Context=MimeTypes
          Type=Scalable
          MinSize=8
          MaxSize=256

          [places/16]
          Size=16
          Context=Places
          MinSize=8
          MaxSize=32
          Type=Scalable

          [places/24]
          Size=24
          Context=Places
          Type=Scalable

          [places/48]
          Size=48
          Context=Places
          MinSize=8
          MaxSize=512
          Type=Scalable

          [Status]
          Context=Status
          Size=96
          MinSize=8
          MaxSize=512
          Type=Scalable

          [Categories]
          Context=Categories
          Size=96
          MinSize=8
          MaxSize=512
          Type=Scalable

          [Devices]
          Size=96
          Context=Devices
          Type=Scalable
          MinSize=8
          MaxSize=512

          [Emblems]
          Size=96
          Context=Emblems
          Type=Scalable
          MinSize=8
          MaxSize=512
          EOF

          for theme in $out/share/icons/*; do
            gtk-update-icon-cache $theme
          done

          runHook postInstall
        '';
      }
    );
    nixfmt = prev.nixfmt-rfc-style;
    # Triggers mass rebuild of system that takes too long. CA derivations may help, but enabling CA derivations by default in nix.nix also triggers mass rebuild, so of no use to me.
    # systemd = prev.systemd.overrideAttrs (_oldAttrs: rec {
    #   mesonFlags =
    #     _oldAttrs.mesonFlags
    #     ++ [
    #       "-Defi-color-normal=cyan,blue"
    #       "-Defi-color-entry=cyan,blue"
    #       "-Defi-color-highlight=blue,cyan"
    #       "-Defi-color-edit=blue,cyan"
    #     ];
    #   # Defaults:
    #   # option('efi-color-normal', type : 'string', value : 'lightgray,black',
    #   #        description : 'general boot loader color in "foreground,background" form, see constants from eficon.h')
    #   # option('efi-color-entry', type : 'string', value : 'lightgray,black',
    #   #        description : 'boot loader color for entries')
    #   # option('efi-color-highlight', type : 'string', value : 'black,lightgray',
    #   #        description : 'boot loader color for selected entries')
    #   # option('efi-color-edit', type : 'string', value : 'black,lightgray',
    #   #        description : 'boot loader color for option line edit')
    #   # Possible colors are;
    #   #EFI_BLACK
    #   #EFI_BLUE
    #   #EFI_GREEN
    #   #EFI_CYAN
    #   #EFI_RED
    #   #EFI_MAGENTA
    #   #EFI_BROWN
    #   #EFI_LIGHTGRAY
    #   #EFI_BRIGHT
    #   #EFI_DARKGRAY
    #   #EFI_LIGHTBLUE
    #   #EFI_LIGHTGREEN
    #   #EFI_LIGHTCYAN
    #   #EFI_LIGHTRED
    #   #EFI_LIGHTMAGENTA
    #   #EFI_YELLOW
    #   #EFI_WHITE
    #   buildInputs = _oldAttrs.buildInputs ++ [prev.libbpf];
    # });
  };
  # When applied, the unstable nixpkgs set (declared in the flake inputs) will
  # be accessible through 'pkgs.unstable'
  # unstable-packages = final: prev: {
  #   unstable = import inputs.nixpkgs-unstable {
  #     inherit (final) system;
  #     config.allowUnfree = true;
  #   };
  # };
}
