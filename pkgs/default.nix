{ inputs, pkgs }:
{
  install-iso = pkgs.callPackage ./install-iso { inherit inputs; };
  # ddns-updater = pkgs.callPackage ./ddns-updater { };
  kidex = pkgs.callPackage ./kidex { };
}
