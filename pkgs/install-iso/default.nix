{ inputs }:
(inputs.nixos-generators.nixosGenerate {
  system = "x86_64-linux";
  modules = [ ../../hosts/install-iso ];
  format = "install-iso";
  specialArgs = {
    inherit inputs;
  }; # allows access to flake inputs in nixos modules
})
