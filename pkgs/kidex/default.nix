{
  lib,
  rustPlatform,
  fetchFromGitHub,
}:

rustPlatform.buildRustPackage rec {
  pname = "kidex";
  version = "0.1.1";

  src = fetchFromGitHub {
    owner = "Kirottu";
    repo = "kidex";
    rev = "v${version}";
    hash = "sha256-LgY4hYJOzGSNZxOK1O4L6A+4/qgv4dhouKo0nLKK25A=";
  };

  cargoHash = "sha256-BkpiJZZ83RrSSmbxM/TBl8rx5wIxLwYDZvFWdTwlUSI=";

  meta = with lib; {
    description = "A simple file indexing service for looking up file locations";
    homepage = "https://github.com/Kirottu/kidex";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ ];
    mainProgram = "kidex";
  };
}
