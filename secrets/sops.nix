{
  pkgs,
  config,
  lib,
  ...
}:
{
  environment.systemPackages = with pkgs; [
    age
    sops
  ];
  # This will add secrets.yml to the nix store
  # You can avoid this by adding a string to the full path instead, i.e.
  # sops.defaultSopsFile = "/root/.sops/secrets/example.yaml";
  sops = {
    defaultSopsFile = ./general.yaml;
    # This is using an age key that is expected to already be in the filesystem
    # sops.age.keyFile = "/home/dae/.config/sops/age/keys.txt";
    # Generate key if not found
    # sops.age.generateKey = true;
    # This will automatically import SSH keys as age keys
    age.sshKeyPaths = [
      (lib.mkIf (config.networking.hostName == "rpi4") "/etc/ssh/ssh_host_ed25519_key")
      (lib.mkIf (config.networking.hostName != "rpi4") "/persistent/etc/ssh/ssh_host_ed25519_key")
    ];
    # Stop importing keys as gpg keys.
    gnupg.sshKeyPaths = [ ];
    # This is the actual specification of the secrets.
    secrets = lib.mkMerge [
      {
        daeHashedPassword = {
          neededForUsers = true;
        };
      }
      (lib.mkIf (config.networking.hostName == "rpi4") {
        ddnsUpdaterConfig = {
          owner = config.services.ddns-updater.user;
          path = "${config.services.ddns-updater.environmentalVariables.DATADIR}/config.json";
          mode = "0500";
          restartUnits = [ "ddns-updater.service" ];
        };
      })
      (lib.mkIf (config.networking.hostName == "swift3") { wireguardLaptopPrivateKey = { }; })
    ];
    templates = lib.mkMerge [
      (lib.mkIf (config.networking.hostName == "swift3") {
        "wireguardConfig.conf" = {
          content = ''
            [Interface]
            Address = 10.10.10.2/24, fc10:10:10::2/64
            ListenPort = 51820
            PrivateKey = ${config.sops.placeholder.wireguardLaptopPrivateKey}
            DNS = 10.10.10.1, fc10:10:10::1

            [Peer]
            PublicKey = xt0cTujq73rlo3d/tBiptJuqvQ40kwll1Etq9RZ9Kwg=
            Endpoint = delliott.xyz:51820
            AllowedIPs = 0.0.0.0/0, ::/0
          '';
          mode = "0700";
          # owner = "dae";
          path = "/home/dae/.config/wireguard/wg0.conf";
        };
      })
    ];
  };
}
