{
  description = "Flake template for use with cpp development";
  # Flake inputs
  inputs = {
    nixpkgs.url = "nixpkgs";
    treefmt-nix.url = "treefmt-nix";
    treefmt-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  # Flake outputs
  outputs =
    inputs:
    let
      forAllSystems = inputs.nixpkgs.lib.genAttrs [
        "aarch64-linux"
        # "i686-linux"
        "x86_64-linux"
        # "aarch64-darwin"
        # "x86_64-darwin"
      ];
      treefmtEval = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        inputs.treefmt-nix.lib.evalModule pkgs {
          config = {
            # Used to find the project root
            projectRootFile = "flake.nix";
            programs = {
              nixfmt.enable = true;
              deadnix.enable = true;
              statix.enable = true;
              clang-format.enable = true;
            };
          };
        }
      );
    in
    {
      # Development environment output
      devShells = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        {
          default = pkgs.mkShell {
            # The Nix packages provided in the environment
            packages = with pkgs; [
              clang-tools # This provides the clang lsp, which is automatically picked up and activated by helix. It also provides clang-format, and some other dev tools
              valgrind
            ];
          };
        }
      );
      formatter = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        treefmtEval.${pkgs.system}.config.build.wrapper
      );
      # for `nix flake check`
      checks = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        {
          formatting = treefmtEval.${pkgs.system}.config.build.checkinputs.self;
        }
      );
      packages = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
          binName = "main";
        in
        {
          default = pkgs.stdenv.mkDerivation {
            name = "main";
            src = inputs.self;
            buildInputs = with pkgs; [ cmake ];
            buildPhase = ''
              make -j $NIX_BUILD_CORES
            '';
            installPhase = ''
              mkdir -p $out/bin
              cp ${binName} $out/bin/
            '';
          };
        }
      );
    };
}
