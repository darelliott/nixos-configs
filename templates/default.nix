{
  cpp = {
    path = ./cpp;
    description = "Cpp development environment";
  };
  python = {
    path = ./python;
    description = "Python development environment";
  };
}
