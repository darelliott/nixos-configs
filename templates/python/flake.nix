{
  description = "Flake template for use with python development";
  # Flake inputs
  inputs = {
    nixpkgs.url = "nixpkgs";
    treefmt-nix.url = "treefmt-nix";
    treefmt-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  # Flake outputs
  outputs =
    inputs:
    let
      forAllSystems = inputs.nixpkgs.lib.genAttrs [
        "aarch64-linux"
        # "i686-linux"
        "x86_64-linux"
        # "aarch64-darwin"
        # "x86_64-darwin"
      ];
      treefmtEval = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        inputs.treefmt-nix.lib.evalModule pkgs {
          config = {
            # Used to find the project root
            projectRootFile = "flake.nix";
            programs = {
              nixfmt.enable = true;
              deadnix.enable = true;
              statix.enable = true;
              black.enable = true;
            };
          };
        }
      );
    in
    {
      # Development environment output
      devShells = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        {
          default = pkgs.mkShell {
            # The Nix packages provided in the environment
            packages = with pkgs; [ ];
          };
        }
      );
      formatter = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        treefmtEval.${pkgs.system}.config.build.wrapper
      );
      # for `nix flake check`
      checks = forAllSystems (
        system:
        let
          pkgs = inputs.nixpkgs.legacyPackages.${system};
        in
        {
          formatting = treefmtEval.${pkgs.system}.config.build.checkinputs.self;
        }
      );
      # packages = forAllSystems (system: let
      #   pkgs = nixpkgs.legacyPackages.${system};
      #   binName = "main";
      # in {
      #   default = pkgs.stdenv.mkDerivation {
      #     name = "main";
      #     src =inputs.self;
      #     buildInputs = [];
      #     buildPhase = "";
      #     installPhase = ''
      #       mkdir -p $out/bin
      #       cp ${binName} $out/bin/
      #     '';
      #   };
      # });
    };
}
